const Theme = {
  fonts: {
    navigationFont: "Frank Ruhl Libre",
    fancyFont: "Akaya Telivigala",
  },
  colors: {
    primary: "#DB5A42",
    primaryText: "#FFF6F0",
    secondary: "#594432",
    loader: "#594432F0",
    error: "#C71700",
    focused: "#59443259",
    messages: {
      background: "#060606",
      warning: "#f9f100",
      error: "#C71700",
      success: "#3bb239",
    },
  },
  breakpoints: {
    mobile: "320px",
    mobileMedium: "375px",
    tablet: "768px",
    desktop: "980px",
    wide: "1300px",
  },
};

export default Theme;
