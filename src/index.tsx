import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

import PageRouter from "./pages/pageRouter";

import "./styles/global.css";
import "bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <PageRouter />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
