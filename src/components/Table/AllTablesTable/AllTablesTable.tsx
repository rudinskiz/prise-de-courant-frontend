import React from "react";
import { Table as BsTable } from "react-bootstrap";
import PropTypes from "prop-types";
import Table from "../../../models/Table";
import NavigationLink from "../../NavigationLink/NavigationLink";

const AllTablesTable = ({
  tables,
  loading,
}: {
  tables: Array<Table>;
  loading: boolean;
}) => {
  return (
    <BsTable>
      <thead>
        <tr>
          <th>Table Name</th>
          <th>Number of Seats</th>
          <th>Smoking Allowed</th>
        </tr>
      </thead>
      <tbody>
        {loading ? (
          <tr>
            <td>Loading...</td>
          </tr>
        ) : tables.length > 0 ? (
          tables.map((table) => {
            return (
              <tr key={table.getId()}>
                <td>
                  <NavigationLink to={`/table/${table.getId()}`}>
                    {table.getName()}
                  </NavigationLink>
                </td>
                <td>{table.getNumberOfSeats()}</td>
                <td>{table.isSmokingAllowed() ? "Yes" : "No"}</td>
              </tr>
            );
          })
        ) : (
          <tr>
            <td colSpan={3}>There are no tables currently.</td>
          </tr>
        )}
      </tbody>
    </BsTable>
  );
};

AllTablesTable.propTypes = {
  tables: PropTypes.arrayOf(PropTypes.instanceOf(Table)),
};

export default AllTablesTable;
