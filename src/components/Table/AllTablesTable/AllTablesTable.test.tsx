import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import AllTablesTable from "./AllTablesTable";
import Table from "../../../models/Table";

const mockTables = [new Table(1, "Jest", "Enzyme", 1, false)];

describe("AllTablesTable.tsx", () => {
  it("should render properly", () => {
    const table = shallow(<AllTablesTable tables={mockTables} />);

    expect(toJSON(table)).toMatchSnapshot();
  });
});
