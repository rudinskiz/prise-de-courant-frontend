import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Spinner } from "react-bootstrap";

import theme from "../../styles/theme";

const LoaderWrapper = styled.div<{ isActive: boolean }>`
  transition: 500ms;
  opacity: ${(props) => (props.isActive ? "1" : "0")};
  position: absolute;
  width: 100%;
  height: ${(props) => (props.isActive ? '100%' : '0px') };
  background: ${theme.colors.loader};
`;

const SpinnerContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
`;

const SpinnerLabel = styled.p`
  transform: translateX(-20%);
`;

const Loader = ({ isLoading, children }: { isLoading: boolean, children: any }) => {
  return (
    <>
    <LoaderWrapper isActive={isLoading}>
      <SpinnerContainer>
        <Spinner animation="border" role="status"></Spinner>
        <SpinnerLabel>Loading...</SpinnerLabel>
      </SpinnerContainer>
    </LoaderWrapper>
    {!isLoading && children}
    </>

  );
};

Loader.propTypes = {
  isLoading: PropTypes.bool.isRequired,
};

export default Loader;
