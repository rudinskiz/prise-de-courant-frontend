import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import Loader from "./Loader";

const mockChildren = "Test";

describe("Loader.tsx", () => {
  it("should render children when not loading", () => {
    const loader = shallow(<Loader isLoading={true}>{mockChildren}</Loader>);

    expect(toJSON(loader)).toMatchSnapshot();
    expect(loader.text()).not.toContain(mockChildren);
  });

  it("should render children when loading", () => {
    const loader = shallow(<Loader isLoading={false}>{mockChildren}</Loader>);

    expect(toJSON(loader)).toMatchSnapshot();
    expect(loader.text()).toContain(mockChildren);
  });
});
