import React from "react";

import { Meta } from "@storybook/react";
import Loader from "./Loader";

export default {
  title: "Components/Loader",
  component: Loader,
} as Meta;

export const Default: React.VFC<{}> = () => <Loader isLoading={true}>.</Loader>;
