import styled from "styled-components";
import theme from "../../../styles/theme";
import ErrorMessage from "../ErrorMessage/ErrorMessage";

const WarningMessage = styled(ErrorMessage)`
  border-left-color: ${theme.colors.messages.warning};
`;

export default WarningMessage;
