import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import WarningMessage from "./WarningMessage";

const mockMessage = "Jest";

describe("WarningMessage.tsx", () => {
  it("should render properly", () => {
    const message = shallow(<WarningMessage>{mockMessage}</WarningMessage>);

    expect(toJSON(message)).toMatchSnapshot();
    expect(message.text()).toBe(mockMessage);
  });
});
