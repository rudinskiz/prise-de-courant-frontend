import React from "react";

import { Meta } from "@storybook/react";
import WarningMessage from "./WarningMessage";

export default {
  title: "Components/Messages/WarningMessage",
  component: WarningMessage,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <WarningMessage>Warning.</WarningMessage>
);
