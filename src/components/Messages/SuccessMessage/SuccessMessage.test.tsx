import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import SuccessMessage from "./SuccessMessage";

const mockMessage = "Jest";

describe("SuccessMessage.tsx", () => {
  it("should render properly", () => {
    const message = shallow(<SuccessMessage>{mockMessage}</SuccessMessage>);

    expect(toJSON(message)).toMatchSnapshot();
    expect(message.text()).toBe(mockMessage);
  });
});
