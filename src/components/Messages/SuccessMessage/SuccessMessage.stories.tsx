import React from "react";

import { Meta } from "@storybook/react";
import SuccessMessage from "./SuccessMessage";

export default {
  title: "Components/Messages/SuccessMessage",
  component: SuccessMessage,
} as Meta;

export const Default: React.VFC<{}> = () => <SuccessMessage>Success.</SuccessMessage>;
