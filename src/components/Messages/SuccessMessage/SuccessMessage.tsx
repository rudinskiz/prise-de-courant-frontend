import styled from "styled-components";
import theme from "../../../styles/theme";
import ErrorMessage from "../ErrorMessage/ErrorMessage";

const SuccessMessage = styled(ErrorMessage)`
  border-left-color: ${theme.colors.messages.success};
`;

export default SuccessMessage;
