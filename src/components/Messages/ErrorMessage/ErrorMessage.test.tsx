import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import ErrorMessage from "./ErrorMessage";

const mockMessage = "Jest";

describe("ErrorMessage.tsx", () => {
  it("should render properly", () => {
    const message = shallow(<ErrorMessage>{mockMessage}</ErrorMessage>);

    expect(toJSON(message)).toMatchSnapshot();
    expect(message.text()).toBe(mockMessage);
  });
});
