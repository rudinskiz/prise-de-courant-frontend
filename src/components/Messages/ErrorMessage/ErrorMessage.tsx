import styled from "styled-components";
import theme from "../../../styles/theme";

const ErrorMessage = styled.div`
  padding: 10px;
  background: ${theme.colors.messages.background};
  font-family: ${theme.fonts.navigationFont};
  color: ${theme.colors.primaryText};
  border-left: 10px solid ${theme.colors.messages.error};
  margin: 10px 0;
`;

export default ErrorMessage;
