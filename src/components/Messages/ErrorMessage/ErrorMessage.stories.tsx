import React from "react";

import { Meta } from "@storybook/react";
import ErrorMessage from "./ErrorMessage";

export default {
  title: "Components/Messages/ErrorMessage",
  component: ErrorMessage,
} as Meta;

export const Default: React.VFC<{}> = () => <ErrorMessage>Error.</ErrorMessage>;
