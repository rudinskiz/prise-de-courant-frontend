import React from "react";

import { Meta } from "@storybook/react";
import FieldValidationError from "./FieldValidationError";

export default {
  title: "Components/Errors/FieldValidationError",
  component: FieldValidationError,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <FieldValidationError isShown>This is an error.</FieldValidationError>
);
