import styled from "styled-components";
import theme from "../../styles/theme";

const FieldValidationError = styled.div<any>`
  display: flex;
  align-items: center;
  transition: all 500ms;
  font-style: italic;
  font-family: ${theme.fonts.navigationFont};
  color: ${theme.colors.error};
  opacity: ${(props) => (props.isShown ? "1" : "0")};
  overflow: hidden;
  max-height: ${(props) => (props.isShown ? "64px" : "0")};
  padding-top: ${(props) => (props.isShown ? "5px" : "0")};
`;

export default FieldValidationError;
