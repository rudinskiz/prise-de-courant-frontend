import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import FieldValidationError from "./FieldValidationError";

const mockMessage = "Jest";

describe("FieldValidationError.tsx", () => {
  it("should render properly", () => {
    const fieldValidationError = shallow(
      <FieldValidationError>{mockMessage}</FieldValidationError>
    );

    expect(toJSON(fieldValidationError)).toMatchSnapshot();
    expect(fieldValidationError.text()).toBe(mockMessage);
  });
});
