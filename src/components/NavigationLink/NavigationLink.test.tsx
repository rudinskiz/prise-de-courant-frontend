import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import NavigationLink from "./NavigationLink";

describe("NavigationLink.tsx", () => {
  it("should render properly", () => {
    const link = shallow(<NavigationLink to={'/'}>Home</NavigationLink>);

    expect(toJSON(link)).toMatchSnapshot();
  });
});
