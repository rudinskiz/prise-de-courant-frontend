import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { darken } from "polished";
import Theme from "../../styles/theme";
import { Link } from "react-router-dom";

const StyledLink = styled(Link)`
  will-change: color;
  transition: color 200ms ease;
  color: ${Theme.colors.primary};
  cursor: pointer;
  width: max-content;

  &:hover {
    color: ${darken(0.65, Theme.colors.primaryText)};
  }
  &:active {
    color: ${darken(0.95, Theme.colors.primaryText)};
  }
`;

const NavigationLink = ({ to, children }) => {
  return <StyledLink to={to}>{children}</StyledLink>;
};

NavigationLink.propTypes = {
  children: PropTypes.any,
  to: PropTypes.string.isRequired,
};

export default NavigationLink;
