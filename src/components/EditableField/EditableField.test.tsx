import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import EditableField from "./EditableField";

describe("EditableField.tsx", () => {
  it("should render properly", () => {
    const field = mount(
      <EditableField
        fieldName="Jest"
        fieldType="text"
        fieldValue="Enzyme"
        onChange={(e) => {}}
      />
    );

    expect(toJSON(field)).toMatchSnapshot();
  });

  it("should update properly", () => {
    const onChange = jest.fn();
    const value = "Hello";

    const field = mount(
      <EditableField
        fieldName="Jest"
        fieldType="text"
        fieldValue="Enzyme"
        onChange={onChange}
      />
    );

    field.find("div").at(1).simulate("click");
    field.update();

    field.find("input").simulate("change", { target: { value } });

    field.find("div").at(1).simulate("click");
    field.update();

    expect(onChange).toBeCalledWith(value);
  });

  it("shouldn't update if nothing has changed", () => {
    const onChange = jest.fn();
    const value = "Hello";

    const field = mount(
      <EditableField
        fieldName="Jest"
        fieldType="text"
        fieldValue="Enzyme"
        onChange={onChange}
      />
    );

    field.find("div").at(1).simulate("click");
    field.update();

    field.find("div").at(1).simulate("click");
    field.update();

    expect(onChange).not.toBeCalled();
  });

  it("should update when 'Enter' is pressed", () => {
    const onChange = jest.fn();
    const value = "Hello";

    const field = mount(
      <EditableField
        fieldName="Jest"
        fieldType="text"
        fieldValue="Enzyme"
        onChange={onChange}
      />
    );

    field.find("div").at(1).simulate("click");
    field.update();

    field.find("input").simulate("change", { target: { value } });
    field.find("input").simulate("keyup", { code: "Enter" });

    field.update();

    expect(onChange).toBeCalledWith(value);
  });

  it("should revert when 'Esc' is pressed", () => {
    const onChange = jest.fn();
    const value = "Hello";

    const field = mount(
      <EditableField
        fieldName="Jest"
        fieldType="text"
        fieldValue="Enzyme"
        onChange={onChange}
      />
    );

    field.find("div").at(1).simulate("click");
    field.update();

    field.find("input").simulate("change", { target: { value } });
    field.find("input").simulate("keyup", { code: "Escape" });

    field.update();

    expect(onChange).not.toBeCalled();
    expect(field.text()).toContain("Enzyme");
  });

  it("should not show the update if the input is empty", () => {
    const onChange = jest.fn();
    const value = "";

    const field = mount(
      <EditableField
        fieldName="Jest"
        fieldType="text"
        fieldValue="Enzyme"
        onChange={onChange}
      />
    );

    field.find("div").at(1).simulate("click");
    field.update();

    field.find("input").simulate("change", { target: { value } });
    field.update();

    const buttonAction = field.find("div").at(1).text();

    field.find("input").simulate("keyup", { code: "Enter" });
    field.update();

    expect(buttonAction).toBe("");
    expect(onChange).not.toBeCalled();
  });
});
