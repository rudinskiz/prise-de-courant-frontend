import React from "react";

import { Meta } from "@storybook/react";
import EditableField from "./EditableField";

export default {
  title: "Components/EditableField",
  component: EditableField,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <EditableField
    fieldName="E-mail"
    fieldValue="hello@world.rs"
    fieldType="text"
  />
);
