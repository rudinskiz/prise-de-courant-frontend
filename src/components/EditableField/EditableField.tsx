import React, { useState } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import Theme from "../../styles/theme";

const FieldLabel = styled.div`
  font-weight: bold;
  margin: 0;
  font-family: ${Theme.fonts.navigationFont};
`;

const FieldValue = styled.span`
  font-weight: normal;
`;

const FieldEdit = styled.div`
  cursor: pointer;
  display: inline;
  margin-left: 10px;
  font-style: italic;
`;

const FieldInput = styled.input``;

const EditableField = ({ fieldName, fieldValue, fieldType, onChange }) => {
  const [editedFieldValue, setEditedFieldValue] = useState("");
  const [editMode, setEditMode] = useState(false);

  return (
    <FieldLabel>
      {fieldName}:{" "}
      {editMode ? (
        <FieldInput
          type={fieldType}
          value={editedFieldValue}
          onKeyUp={(e) => {
            if (e.code === "Enter") {
              // Check if we even need to update.
              if (
                editedFieldValue !== fieldValue &&
                editedFieldValue.length > 0
              ) {
                onChange(editedFieldValue);
              }
              setEditMode(false);
            } else if (e.code === "Escape") {
              setEditMode(false);
            }
          }}
          onChange={(e) => {
            setEditedFieldValue(e.target.value);
          }}
        />
      ) : (
        <FieldValue>{fieldValue}</FieldValue>
      )}
      {onChange && (
        <FieldEdit
          onClick={() => {
            if (editMode) {
              // Check if we even need to update.
              if (
                editedFieldValue !== fieldValue &&
                editedFieldValue.length > 0
              ) {
                onChange(editedFieldValue);
              }
            } else {
              setEditedFieldValue(fieldValue);
            }
            setEditMode(!editMode);
          }}
        >
          {editMode ? (editedFieldValue.length > 0 ? "Update" : "") : "Edit"}
        </FieldEdit>
      )}
    </FieldLabel>
  );
};

EditableField.defaultProps = {
  onChange: false,
  fieldType: "text",
};

EditableField.propTypes = {
  onChange: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  fieldName: PropTypes.string.isRequired,
  fieldType: PropTypes.string,
  fieldValue: PropTypes.string.isRequired,
};

export default EditableField;
