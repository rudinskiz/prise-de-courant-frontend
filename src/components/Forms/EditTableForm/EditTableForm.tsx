import React from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import { AddTableButton, StyledForm } from "./style";
import { Form } from "react-bootstrap";
import FieldValidationError from "../../Errors/FieldValidationError";

const EditTableForm = ({ onSubmitHandler, initialValues }) => {

  return (
    <Formik
      initialValues={initialValues}
      validate={({ tableName, tableDescription, numberOfSeats }) => {
        // Errors object.
        const errors: any = {};
        if (tableName.length < 1) {
          errors.tableName = "Name can't be empty.";
        }
        if (tableDescription.length < 1) {
          errors.tableDescription = "Description can't be empty.";
        }
        if (numberOfSeats < 1) {
          errors.numberOfSeats = "Number of seats must be greater than 0.";
        }
        return errors;
      }}
      onSubmit={onSubmitHandler}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <StyledForm onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Label>Table name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Table name..."
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.tableName}
              name="tableName"
            />
            <FieldValidationError
              isShown={errors.tableName && touched.tableName}
            >
              {errors.tableName}
            </FieldValidationError>
          </Form.Group>
          <Form.Group>
            <Form.Label>Table description</Form.Label>
            <Form.Control
              placeholder="Table description..."
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.tableDescription}
              name="tableDescription"
              as="textarea"
              rows={4}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Number of seats</Form.Label>
            <Form.Control
              type="number"
              min="1"
              placeholder="Number of seats..."
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.numberOfSeats}
              name="numberOfSeats"
            />
          </Form.Group>
          <Form.Group>
            <Form.Check
              onChange={handleChange}
              onBlur={handleBlur}
              checked={values.smokingAllowed}
              type="checkbox"
              label="Smoking allowed"
              name="smokingAllowed"
            />
          </Form.Group>
          <AddTableButton
            variant="primary"
            type="submit"
            disabled={isSubmitting}
          >
            Update table
          </AddTableButton>
        </StyledForm>
      )}
    </Formik>
  );
};

EditTableForm.propTypes = {
  onSubmitHandler: PropTypes.func.isRequired,
  initialValues: PropTypes.shape({
    tableName: PropTypes.string,
    tableDescription: PropTypes.string,
    numberOfSeats: PropTypes.number,
    smokingAllowed: PropTypes.bool,
  }).isRequired,
};

export default EditTableForm;
