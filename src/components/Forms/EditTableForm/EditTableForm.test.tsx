import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import EditTableForm from "./EditTableForm";

describe("EditTableForm.tsx", () => {
  it("should render properly", () => {
    const loginForm = shallow(
      <EditTableForm
        onSubmitHandler={() => null}
        initialValues={{
          tableName: "",
          tableDescription: "",
          numberOfSeats: 1,
          smokingAllowed: true,
        }}
      />
    );

    expect(toJSON(loginForm)).toMatchSnapshot();
  });

  it("should validate form before calling the API", async () => {
    const submitHandler = jest.fn();

    const loginForm = mount(
      <EditTableForm
        onSubmitHandler={submitHandler}
        initialValues={{
          tableName: "",
          tableDescription: "",
          numberOfSeats: 1,
          smokingAllowed: true,
        }}
      />
    );
    loginForm.find("button").simulate("click");

    expect(submitHandler).not.toBeCalled();
  });
});
