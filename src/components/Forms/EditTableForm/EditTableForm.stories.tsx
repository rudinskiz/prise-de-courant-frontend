import React from "react";

import { Meta } from "@storybook/react";
import EditTableForm from "./EditTableForm";

export default {
  title: "Components/Forms/EditTableForm",
  component: EditTableForm,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <EditTableForm
    onSubmitHandler={() => null}
    initialValues={{
      tableName: "",
      tableDescription: "",
      numberOfSeats: 1,
      smokingAllowed: true,
    }}
  />
);
