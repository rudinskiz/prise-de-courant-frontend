import React from "react";
import { Col, Form, Row } from "react-bootstrap";
import { Formik } from "formik";
import { validate } from "email-validator";
import PropTypes from "prop-types";
import { PasswordResetRequestButton, StyledForm } from "./style";
import FieldValidationError from "../../Errors/FieldValidationError";
import ErrorMessage from "../../Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../Messages/SuccessMessage/SuccessMessage";

const PasswordResetRequestForm = ({ onSubmitHandler, message }) => {
  return (
    <>
      {message.message &&
        (message.status === "error" ? (
          <ErrorMessage>{message.message}</ErrorMessage>
        ) : (
          <SuccessMessage>{message.message}</SuccessMessage>
        ))}
      <Formik
        initialValues={{
          email: "",
        }}
        validate={({ email }) => {
          // Errors object.
          const errors: any = {};
          // Validate e-mail.
          if (!validate(email)) {
            errors.email = "Invalid e-mail address.";
          }

          return errors;
        }}
        onSubmit={onSubmitHandler}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <StyledForm onSubmit={handleSubmit}>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>E-mail</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="E-mail address..."
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    name="email"
                  />
                  <FieldValidationError isShown={errors.email && touched.email}>
                    {errors.email}
                  </FieldValidationError>
                </Form.Group>
              </Col>
            </Row>
            <PasswordResetRequestButton
              variant="primary"
              type="submit"
              disabled={isSubmitting}
            >
              Reset password
            </PasswordResetRequestButton>
          </StyledForm>
        )}
      </Formik>
    </>
  );
};

PasswordResetRequestForm.propTypes = {
  onSubmitHandler: PropTypes.func.isRequired,
  message: PropTypes.shape({
    status: PropTypes.string,
    message: PropTypes.string,
  }),
};

export default PasswordResetRequestForm;
