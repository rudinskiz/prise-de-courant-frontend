import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import PasswordResetRequestForm from "./PasswordResetRequestForm";

describe("PasswordResetRequestForm.tsx", () => {
  it("should render properly", () => {
    const loginForm = shallow(
      <PasswordResetRequestForm
        onSubmitHandler={() => null}
        message={{ status: "", message: "" }}
      />
    );

    expect(toJSON(loginForm)).toMatchSnapshot();
  });

  it("should validate form before calling the API", async () => {
    const submitHandler = jest.fn();

    const loginForm = mount(
      <PasswordResetRequestForm
        onSubmitHandler={submitHandler}
        message={{ status: "", message: "" }}
      />
    );
    loginForm.find("button").simulate("click");

    expect(submitHandler).not.toBeCalled();
  });
});
