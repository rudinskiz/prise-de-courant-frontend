import React from "react";

import { Meta } from "@storybook/react";
import PasswordResetRequestForm from "./PasswordResetRequestForm";

export default {
  title: "Components/Forms/PasswordResetRequestForm",
  component: PasswordResetRequestForm,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <PasswordResetRequestForm onSubmitHandler={() => null} />
);
