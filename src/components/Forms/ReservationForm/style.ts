import { Button } from "react-bootstrap";
import styled from "styled-components";
import theme from "../../../styles/theme";

const ActionButton = styled(Button)`
  transition: all 500ms;
  width: 100%;
  background-color: ${theme.colors.primary};
  border-color: ${theme.colors.primary};
  font-family: ${theme.fonts.navigationFont};
  margin-top: 1rem;

  &:hover {
    background-color: ${theme.colors.secondary};
    border-color: ${theme.colors.secondary};
  }
  &:active {
    background-color: ${theme.colors.secondary} !important;
    border-color: ${theme.colors.secondary} !important;
  }
  &:focus {
    box-shadow: 0 0 0 0.2rem ${theme.colors.focused};
    background-color: ${theme.colors.secondary};
    border-color: ${theme.colors.secondary};
  }
  &:disabled {
    cursor: default;
    background-color: ${theme.colors.primary};
    border-color: ${theme.colors.primary};
  }
`;

export { ActionButton };
