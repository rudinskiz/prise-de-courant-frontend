import React, { useEffect, useState } from "react";
import { Container, Form } from "react-bootstrap";
import useLoader from "../../../hooks/useLoader";
import PageTitle from "../../PageTitle/PageTitle";
import Loader from "../../Loader/Loader";
import {
  getReservationByCode,
  setComment,
} from "../../../utils/api/reservation";
import ErrorMessage from "../../Messages/ErrorMessage/ErrorMessage";
import { ActionButton } from "../ProfileForm/style";
import EditableField from "../../EditableField/EditableField";
import useAuthentication from "../../../hooks/useAuthentication";
import { useHistory } from "react-router-dom";
import Title from "../../Title/Title";

const ReservationForm = () => {
  const [reservationError, setReservationError] = useState("");
  const [reservationCode, setReservationCode] = useState("");
  const [reservation, setReservation] = useState(null);
  const { getToken } = useAuthentication();
  const history = useHistory();

  const [loading, setLoading] = useLoader();

  const onSubmitForm = (e) => {
    e.preventDefault();
    setReservationError("");
    setLoading(true);
    getReservationByCode(reservationCode).then((reservation) => {
      console.log(reservation);
      setReservation(reservation);
      if (reservation === null) {
        setReservationError("No reservation found.");
      }
      setLoading(false);
    });
  };

  const onCommentChange = (comment) => {
    setReservationError("");
    setLoading(true);
    setComment({
      comment,
      reservationId: reservation.getCode(),
      token: getToken()?.getValue() ?? "",
    }).then((status) => {
      setLoading(false);
      if (!status) {
        setReservationError("Failed to update the comment.");
      } else {
        history.go(0);
      }
    });
  };

  useEffect(() => {
    setTimeout(() => setLoading(false), 1000);
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <Title
        title={
          reservation === null
            ? "Reservation Check"
            : reservation.getReservedTable().getName()
        }
      />
      <Loader isLoading={loading}>
        <Container>
          <PageTitle align="left">Reservation Check</PageTitle>
          {reservationError && <ErrorMessage>{reservationError}</ErrorMessage>}
          {reservation !== null && (
            <>
              <p>
                <b>Table:</b> {reservation.getReservedTable().getName()}
              </p>
              <p>
                <b>Reserved by:</b> {reservation.getUser().getName()}
              </p>
              <p>
                <b>Reservation time:</b>{" "}
                {reservation.getStartDate().toFormat("dd/MM/yyyy HH:mm")} -{" "}
                {reservation.getEndDate().toFormat("dd/MM/yyyy HH:mm")}
              </p>
              <EditableField
                fieldName="Comment"
                fieldType="text"
                fieldValue={reservation.getComment()}
                onChange={onCommentChange}
              />
              <hr />
            </>
          )}
          {reservation?.hasDiscount() && (
            <p>This reservation is eligible for a discount.</p>
          )}
          <Form onSubmit={onSubmitForm}>
            <Form.Group>
              <Form.Label>Reservation code</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter your reservation code..."
                onChange={(e) => setReservationCode(e.target.value)}
                value={reservationCode}
              />
            </Form.Group>
            <ActionButton variant="primary" type="submit">
              Check
            </ActionButton>
          </Form>
        </Container>
      </Loader>
    </>
  );
};

export default ReservationForm;
