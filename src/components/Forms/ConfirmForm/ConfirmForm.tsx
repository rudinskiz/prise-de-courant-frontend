import React from "react";
import PropTypes from "prop-types";
import { ButtonWrapper, ConfirmButton, StyledForm } from "./style";
import { Form } from "react-bootstrap";

const ConfirmForm = ({ action, cancel }) => {
  return (
    <StyledForm>
      <Form.Group>
        <Form.Label>Are you sure?</Form.Label>
        <ButtonWrapper>
          <ConfirmButton onClick={action}>Yes</ConfirmButton>
          <ConfirmButton onClick={cancel}>No</ConfirmButton>
        </ButtonWrapper>
      </Form.Group>
    </StyledForm>
  );
};

ConfirmForm.propTypes = {
  action: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
};

export default ConfirmForm;
