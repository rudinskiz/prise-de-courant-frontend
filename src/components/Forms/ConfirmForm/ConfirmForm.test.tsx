import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import ConfirmForm from "./ConfirmForm";

describe("AddTableForm.tsx", () => {
  it("should render properly", () => {
    const confirmForm = shallow(
      <ConfirmForm action={() => null} cancel={() => null} />
    );

    expect(toJSON(confirmForm)).toMatchSnapshot();
  });

  it("should call the action handler", async () => {
    const submitHandler = jest.fn();

    const confirmForm = mount(
      <ConfirmForm action={submitHandler} cancel={() => null} />
    );
    confirmForm.find("button").at(0).simulate("click");

    expect(submitHandler).toBeCalled();
  });

  it("should call the cancel handler", async () => {
    const submitHandler = jest.fn();

    const confirmForm = mount(
      <ConfirmForm action={() => null} cancel={submitHandler} />
    );
    confirmForm.find("button").at(1).simulate("click");

    expect(submitHandler).toBeCalled();
  });
});
