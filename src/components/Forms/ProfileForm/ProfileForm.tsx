import { useContext, useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import useLoader from "../../../hooks/useLoader";
import User from "../../../models/User";
import UserContext from "../../../utils/contexts/UserContext";
import EditableField from "../../EditableField/EditableField";
import Loader from "../../Loader/Loader";
import PageTitle from "../../PageTitle/PageTitle";
import { ActionButton, ButtonWrapper } from "./style";
import { updateUserInformation } from "../../../utils/api/user";
import { useHistory } from "react-router";
import ErrorMessage from "../../Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../Messages/SuccessMessage/SuccessMessage";
import useAuthentication from "../../../hooks/useAuthentication";
import Constants from "../../../utils/constants";
import ReservationsList from "../../ReservationsList/ReservationsList";
import MasqueradeForm from "../MasqueradeForm/MasqueradeForm";
import Title from "../../Title/Title";

const ProfileForm = () => {
  const history = useHistory();
  const [profileUpdateMessage, setProfileUpdateMessage] = useState({
    status: "",
    message: "",
  });
  const [buttonsActive, setButtonsActive] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [updatedFields, setUpdatedFields] = useState({});
  const [loading, setLoading] = useLoader();
  const userContext = useContext(UserContext);
  const { getToken } = useAuthentication();

  const { user }: { user: User } = userContext;

  const getFieldValue = (field: string, defaultValue: string) => {
    if (field in updatedFields) {
      return updatedFields[field];
    }
    return defaultValue;
  };

  const onSaveChanges = () => {
    setIsSubmitting(true);
    // Call the API.
    updateUserInformation(updatedFields, getToken()?.getValue() ?? "").then(
      (result) => {
        if (result) {
          setProfileUpdateMessage({
            status: "success",
            message: "Information updated. Please wait...",
          });
          // Reload the page.
          setTimeout(() => {
            history.go(0);
          }, 5000);
        } else {
          setProfileUpdateMessage({
            status: "error",
            message: "An error has occurred. Please try again.",
          });
          setIsSubmitting(false);
        }
      }
    );
  };

  const onRevertChanges = () => {
    setUpdatedFields({});
    setProfileUpdateMessage({
      status: "",
      message: "",
    });
  };

  useEffect(() => {
    // Illusion of loading.
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, [setLoading]);

  useEffect(() => {
    setButtonsActive(Object.keys(updatedFields).length > 0);
  }, [updatedFields]);

  return (
    <>
      <Title title={loading ? "Loading profile..." : user.getName()} />
      <Loader isLoading={loading}>
        <Container>
          <PageTitle align={"left"}>
            {getFieldValue("name", user.getName())}
          </PageTitle>
          {profileUpdateMessage.message &&
            (profileUpdateMessage.status === "error" ? (
              <ErrorMessage>{profileUpdateMessage.message}</ErrorMessage>
            ) : (
              <SuccessMessage>{profileUpdateMessage.message}</SuccessMessage>
            ))}
          <EditableField
            fieldName="Name"
            fieldValue={getFieldValue("name", user.getName())}
            onChange={(name) => {
              setUpdatedFields({
                ...updatedFields,
                name,
              });
            }}
          />
          <EditableField
            fieldName="E-mail"
            fieldValue={getFieldValue("email", user.getEmail())}
            fieldType="email"
            onChange={(email) => {
              setUpdatedFields({
                ...updatedFields,
                email,
              });
            }}
          />
          <EditableField
            fieldName="Password"
            fieldValue={getFieldValue("password", "Your current password.")}
            fieldType="password"
            onChange={(password) => {
              setUpdatedFields({
                ...updatedFields,
                password,
              });
            }}
          />
          <EditableField
            fieldName="Phone"
            fieldValue={getFieldValue("phoneNumber", user.getPhoneNumber())}
            fieldType="tel"
            onChange={(phoneNumber) => {
              setUpdatedFields({
                ...updatedFields,
                phoneNumber,
              });
            }}
          />
          <EditableField fieldName="Role" fieldValue={user.getRoleName()} />
          <ButtonWrapper isShown={buttonsActive}>
            <ActionButton
              disabled={isSubmitting || !buttonsActive}
              onClick={onSaveChanges}
            >
              Save changes
            </ActionButton>
            <ActionButton
              disabled={isSubmitting || !buttonsActive}
              onClick={onRevertChanges}
            >
              Revert changes
            </ActionButton>
          </ButtonWrapper>
          {user.getRoleName() === Constants.auth.adminRole && (
            <MasqueradeForm />
          )}
          <hr />
          <ReservationsList />
        </Container>
      </Loader>
    </>
  );
};

export default ProfileForm;
