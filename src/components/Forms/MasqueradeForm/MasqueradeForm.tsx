import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import useAuthentication from "../../../hooks/useAuthentication";
import { masquerade } from "../../../utils/api/user";
import ErrorMessage from "../../Messages/ErrorMessage/ErrorMessage";
import PageTitle from "../../PageTitle/PageTitle";
import { ActionButton, StyledForm } from "./style";

const MasqueradeForm = () => {
  const [masqueradeError, setMasqueradeError] = useState(null);
  const [masqueradeEmail, setMasqueradeEmail] = useState(null);
  const history = useHistory();

  const { setToken, getToken } = useAuthentication();

  const onSubmitHandler = () => {
    setMasqueradeError(null);
    masquerade(masqueradeEmail, getToken().getValue()).then((token) => {
      if (token === null) {
        setMasqueradeError(`Could not masquerade as '${masqueradeEmail}'.`);
      } else {
        setToken(token);
        history.go(0);
      }
    });
  };

  return (
    <StyledForm>
      <PageTitle align={"left"}>Masquerade as:</PageTitle>
      {masqueradeError !== null && (
        <ErrorMessage>{masqueradeError}</ErrorMessage>
      )}
      <input
        type="email"
        value={masqueradeEmail}
        onChange={(e) => setMasqueradeEmail(e.target.value)}
        placeholder="Please enter an e-mail..."
      />
      <ActionButton onClick={onSubmitHandler}>Masquerade</ActionButton>
    </StyledForm>
  );
};

export default MasqueradeForm;
