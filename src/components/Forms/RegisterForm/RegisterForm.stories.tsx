import React from "react";

import { Meta } from "@storybook/react";
import RegisterForm from "./RegisterForm";

export default {
  title: "Components/Forms/RegisterForm",
  component: RegisterForm,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <RegisterForm onSubmitHandler={() => null} />
);
