import React, { useState } from "react";
import { Col, Form, Row } from "react-bootstrap";
import { Formik } from "formik";
import { validate } from "email-validator";
import { PhoneNumberUtil } from "google-libphonenumber";
import PropTypes from "prop-types";
import { RegisterButton, StyledForm } from "./style";
import FieldValidationError from "../../Errors/FieldValidationError";
import ActionLink from "../../ActionLink/ActionLink";
import ResendVerificationForm from "../ResendVerificationForm/ResendVerificationForm";
import { requestVerificationResend } from "../../../utils/api/user";
import Modal from "../../Modal/Modal";

const phoneNumberUtil = PhoneNumberUtil.getInstance();

const RegisterForm = ({ onSubmitHandler }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [resendVerificationMessage, setResendVerificationMessage] = useState({
    status: "",
    message: "",
  });

  const onSubmitResendVerificationRequest = (
    values: { email },
    { setSubmitting }
  ) => {
    // Try to request a verification resend.
    const { email } = values;
    requestVerificationResend(email).then((result) => {
      if (result) {
        setResendVerificationMessage({
          status: "success",
          message: "Verification e-mail sent. Please check your inbox.",
        });
      } else {
        setResendVerificationMessage({
          status: "error",
          message: "An error has occurred. Please try again.",
        });
      }

      setSubmitting(false);
    });
  };

  return (
    <>
      <Modal
        isOpen={modalOpen}
        onAfterOpen={() =>
          setResendVerificationMessage({ status: "", message: "" })
        }
        setModalState={setModalOpen}
        ariaHideApp={false}
      >
        <ResendVerificationForm
          onSubmitHandler={onSubmitResendVerificationRequest}
          message={resendVerificationMessage}
        />
      </Modal>
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          phoneNumber: "",
        }}
        validate={({ firstName, lastName, email, password, phoneNumber }) => {
          // Errors object.
          const errors: any = {};
          // Validate the lengths.
          if (firstName.length < 2 || firstName.length > 12) {
            errors.firstName =
              "First name should be between 2 and 12 characters long.";
          }
          if (lastName.length < 2 || lastName.length > 12) {
            errors.lastName =
              "Last name should be between 2 and 12 characters long.";
          }
          if (password.length < 8 || password.length > 24) {
            errors.password =
              "Password should be between 8 and 24 characters long.";
          }
          // Validate e-mail.
          if (!validate(email)) {
            errors.email = "Invalid e-mail address.";
          }
          // Validate phone number.
          try {
            if (
              !phoneNumberUtil.isValidNumber(
                phoneNumberUtil.parse(phoneNumber, "RS")
              )
            ) {
              throw Error;
            }
          } catch {
            errors.phoneNumber = "Invalid phone number.";
          }

          return errors;
        }}
        onSubmit={onSubmitHandler}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <StyledForm onSubmit={handleSubmit}>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>First name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="First name..."
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.firstName}
                    name="firstName"
                  />
                  <FieldValidationError
                    isShown={errors.firstName && touched.firstName}
                  >
                    {errors.firstName}
                  </FieldValidationError>
                </Form.Group>
              </Col>
              <Col>
                <Form.Group>
                  <Form.Label>Last name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Last name..."
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lastName}
                    name="lastName"
                  />
                  <FieldValidationError
                    isShown={errors.lastName && touched.lastName}
                  >
                    {errors.lastName}
                  </FieldValidationError>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>E-mail</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="E-mail address..."
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    name="email"
                  />
                  <FieldValidationError isShown={errors.email && touched.email}>
                    {errors.email}
                  </FieldValidationError>
                </Form.Group>
              </Col>
              <Col>
                <Form.Group>
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password..."
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                    name="password"
                  />
                  <FieldValidationError
                    isShown={errors.password && touched.password}
                  >
                    {errors.password}
                  </FieldValidationError>
                </Form.Group>
              </Col>
            </Row>
            <Form.Group>
              <Form.Label>Phone number</Form.Label>
              <Form.Control
                type="tel"
                placeholder="Phone number..."
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phoneNumber}
                name="phoneNumber"
              />
              <FieldValidationError
                isShown={errors.phoneNumber && touched.phoneNumber}
              >
                {errors.phoneNumber}
              </FieldValidationError>
            </Form.Group>
            <ActionLink onClick={() => setModalOpen(true)}>
              Resend verification e-mail
            </ActionLink>
            <RegisterButton
              variant="primary"
              type="submit"
              disabled={isSubmitting}
            >
              Register
            </RegisterButton>
          </StyledForm>
        )}
      </Formik>
    </>
  );
};

RegisterForm.propTypes = {
  onSubmitHandler: PropTypes.func.isRequired,
};

export default RegisterForm;
