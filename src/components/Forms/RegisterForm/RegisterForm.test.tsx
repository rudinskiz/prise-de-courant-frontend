import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import RegisterForm from "./RegisterForm";

describe("RegisterForm.tsx", () => {
  it("should render properly", () => {
    const registerForm = shallow(<RegisterForm onSubmitHandler={() => null} />);

    expect(toJSON(registerForm)).toMatchSnapshot();
  });

  it("should validate form before calling the API", async () => {
    const submitHandler = jest.fn();

    const registerForm = mount(
      <RegisterForm onSubmitHandler={submitHandler} />
    );
    registerForm.find("button").simulate("click");

    expect(submitHandler).not.toBeCalled();
  });
});
