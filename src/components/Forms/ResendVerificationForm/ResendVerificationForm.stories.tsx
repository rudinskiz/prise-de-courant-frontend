import React from "react";

import { Meta } from "@storybook/react";
import ResendVerificationForm from "./ResendVerificationForm";

export default {
  title: "Components/Forms/ResendVerificationForm",
  component: ResendVerificationForm,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <ResendVerificationForm onSubmitHandler={() => null} />
);
