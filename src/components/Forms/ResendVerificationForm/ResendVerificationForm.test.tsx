import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import ResendVerificationForm from "./ResendVerificationForm";

describe("ResendVerificationForm.tsx", () => {
  it("should render properly", () => {
    const resendVerificationForm = shallow(
      <ResendVerificationForm
        onSubmitHandler={() => null}
        message={{ status: "", message: "" }}
      />
    );

    expect(toJSON(resendVerificationForm)).toMatchSnapshot();
  });

  it("should validate form before calling the API", async () => {
    const submitHandler = jest.fn();

    const resendVerificationForm = mount(
      <ResendVerificationForm
        onSubmitHandler={submitHandler}
        message={{ status: "", message: "" }}
      />
    );
    resendVerificationForm.find("button").simulate("click");

    expect(submitHandler).not.toBeCalled();
  });
});
