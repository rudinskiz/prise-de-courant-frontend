import { DateTime } from "luxon";
import { useState } from "react";
import QRCode from "react-qr-code";
import useAuthentication from "../../../hooks/useAuthentication";
import { addDiscount, createReservation } from "../../../utils/api/reservation";
import ErrorMessage from "../../Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../Messages/SuccessMessage/SuccessMessage";
import PageTitle from "../../PageTitle/PageTitle";
import {
  ActionButton,
  PickerWrapper,
  QRCodeWrapper,
  StyledForm,
} from "./style";
import * as Survey from "survey-react";
import "survey-react/survey.css";

const SurveyJSON = {
  title: "Prise De Courant Survey",
  logoPosition: "right",
  pages: [
    {
      name: "page1",
      elements: [
        {
          type: "rating",
          name: "question1",
          title: "How would you rate our website?",
        },
        {
          type: "dropdown",
          name: "question2",
          title: "Gender",
          choices: [
            { value: "male", text: "Male" },
            { value: "female", text: "Female" },
            { value: "other", text: "Other" },
          ],
        },
        {
          type: "dropdown",
          name: "question3",
          title: "How did you discover us?",
          choices: [
            { value: "wordofmouth", text: "From friends, family, etc." },
            { value: "searchengine", text: "Google, Bing, etc." },
            { value: "physical", text: "Flyers, posters, etc." },
          ],
        },
        { type: "signaturepad", name: "question4", title: "Please sign" },
      ],
    },
  ],
};

Survey.StylesManager.applyTheme("orange");

const BookTableForm = ({ table }) => {
  const [reservationDate, setReservationDate] = useState(null);
  const [reservationTime, setReservationTime] = useState(null);
  const [reservationDuration, setReservationDuration] = useState(1);
  const [reservationMessage, setReservationMessage] = useState({
    status: "",
    message: "",
    code: "",
  });
  const [wantDiscount, setWantDiscount] = useState(false);
  const { getToken } = useAuthentication();

  const onSurveyComplete = () => {
    addDiscount(reservationMessage.code).then((res) => {
      if (res) {
        alert("Discount applied successfully");
      } else {
        alert("Could not apply discount");
      }
    });
  };

  const onSubmitHandler = (e) => {
    e.preventDefault();
    setReservationMessage({
      code: "",
      status: "",
      message: "",
    });
    if (reservationDate === null || reservationTime === null) {
      setReservationMessage({
        status: "error",
        message: "Please specify the reservation start date.",
        code: "",
      });
      return;
    }
    // Create a datetime object.
    const reservationStart = DateTime.fromFormat(
      `${reservationDate} ${reservationTime}`,
      "yyyy-MM-dd HH:mm"
    );
    if (!reservationStart.isValid) {
      setReservationMessage({
        status: "error",
        message: "Please specify the reservation start date.",
        code: "",
      });
      return;
    }
    // Check if date is in the past.
    if (reservationStart.diffNow("hours").hours < 2) {
      setReservationMessage({
        status: "error",
        message: "Reservations must be made at least 2 hours in advance.",
        code: "",
      });
      return;
    }
    // Try to reserve the table.
    createReservation(
      reservationStart,
      reservationStart.plus({ hours: reservationDuration }),
      getToken()?.getValue() ?? "",
      table
    ).then((code) => {
      if (code) {
        setReservationMessage({
          status: "success",
          message: `Reservation created. Your code is: ${code}. You should've recieved an SMS message as well.`,
          code,
        });
      } else {
        setReservationMessage({
          status: "error",
          message: "Could not reserve the table at that specific time.",
          code: "",
        });
      }
    });
  };

  return (
    <>
      <PageTitle align={"left"}>Reservation</PageTitle>
      {reservationMessage.message &&
        (reservationMessage.status === "error" ? (
          <ErrorMessage>{reservationMessage.message}</ErrorMessage>
        ) : (
          <>
            {wantDiscount ? (
              <Survey.Survey json={SurveyJSON} onComplete={onSurveyComplete} />
            ) : (
              <p>
                Want a discount?{" "}
                <a href="#" onClick={() => setWantDiscount(true)}>
                  Click here to fill out the survey.
                </a>
              </p>
            )}
            <QRCodeWrapper>
              <QRCode value={reservationMessage.code} />
            </QRCodeWrapper>
            <SuccessMessage>{reservationMessage.message}</SuccessMessage>
          </>
        ))}
      <StyledForm>
        <PickerWrapper>
          <input
            type="date"
            value={reservationDate}
            onChange={(e) => setReservationDate(e.target.value)}
          />
          <input
            type="time"
            value={reservationTime}
            onChange={(e) => setReservationTime(e.target.value)}
          />
        </PickerWrapper>
        <select
          value={reservationDuration}
          onChange={(e) => {
            setReservationDuration(Number(e.target.value));
          }}
        >
          {[1, 2, 3, 4].map((hr) => (
            <option key={hr} value={hr}>
              {hr} hour(s)
            </option>
          ))}
        </select>
        <ActionButton onClick={onSubmitHandler}>Reserve table</ActionButton>
      </StyledForm>
    </>
  );
};

export default BookTableForm;
