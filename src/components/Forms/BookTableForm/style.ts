import { Button, Form } from "react-bootstrap";
import styled from "styled-components";
import theme from "../../../styles/theme";

const ActionButton = styled(Button)`
  margin-top: 10px;
  transition: all 500ms;
  width: 100%;
  background-color: ${theme.colors.primary};
  border-color: ${theme.colors.primary};
  font-family: ${theme.fonts.navigationFont};

  &:hover {
    background-color: ${theme.colors.secondary};
    border-color: ${theme.colors.secondary};
  }
  &:active {
    background-color: ${theme.colors.secondary} !important;
    border-color: ${theme.colors.secondary} !important;
  }
  &:focus {
    box-shadow: 0 0 0 0.2rem ${theme.colors.focused};
    background-color: ${theme.colors.secondary};
    border-color: ${theme.colors.secondary};
  }
`;

const StyledForm = styled(Form)`
  font-family: ${theme.fonts.navigationFont};
  & > select {
    width: 100%;
  }
`;

const PickerWrapper = styled.div`
  display: flex;
  gap: 10px;
  margin-bottom: 1rem;

  & > input {
    width: 50%;
  }
`;

export const QRCodeWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export { ActionButton, StyledForm, PickerWrapper };
