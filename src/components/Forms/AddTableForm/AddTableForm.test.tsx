import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import AddTableForm from "./AddTableForm";

describe("AddTableForm.tsx", () => {
  it("should render properly", () => {
    const loginForm = shallow(<AddTableForm onSubmitHandler={() => null} />);

    expect(toJSON(loginForm)).toMatchSnapshot();
  });

  it("should validate form before calling the API", async () => {
    const submitHandler = jest.fn();

    const loginForm = mount(<AddTableForm onSubmitHandler={submitHandler} />);
    loginForm.find("button").simulate("click");

    expect(submitHandler).not.toBeCalled();
  });
});
