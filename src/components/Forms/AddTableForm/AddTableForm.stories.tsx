import React from "react";

import { Meta } from "@storybook/react";
import AddTableForm from "./AddTableForm";

export default {
  title: "Components/Forms/AddTableForm",
  component: AddTableForm,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <AddTableForm onSubmitHandler={() => null} />
);
