import React, { useState } from "react";
import { Form } from "react-bootstrap";
import { Formik } from "formik";
import { validate } from "email-validator";
import PropTypes from "prop-types";
import { requestPasswordReset } from "../../../utils/api/user";
import FieldValidationError from "../../Errors/FieldValidationError";
import { LoginButton, StyledForm } from "./style";
import ActionLink from "../../ActionLink/ActionLink";
import PasswordResetRequestForm from "../PasswordResetRequestForm/PasswordResetRequestForm";
import Modal from "../../Modal/Modal";

const LoginForm = ({ onSubmitHandler }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [passwordResetMessage, setPasswordResetMessage] = useState({
    status: "",
    message: "",
  });

  const onSubmitPasswordResetRequest = (
    values: { email },
    { setSubmitting }
  ) => {
    // Try to request a password reset.
    const { email } = values;
    requestPasswordReset(email).then((result) => {
      if (result) {
        setPasswordResetMessage({
          status: "success",
          message: "Password request sent. Please check your inbox.",
        });
      } else {
        setPasswordResetMessage({
          status: "error",
          message: "An error has occurred. Please try again.",
        });
      }

      setSubmitting(false);
    });
  };

  return (
    <>
      <Modal
        isOpen={modalOpen}
        onAfterOpen={() => setPasswordResetMessage({ status: "", message: "" })}
        setModalState={setModalOpen}
        ariaHideApp={false}
      >
        <PasswordResetRequestForm
          onSubmitHandler={onSubmitPasswordResetRequest}
          message={passwordResetMessage}
        />
      </Modal>
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validate={({ email, password }) => {
          // Errors object.
          const errors: any = {};
          // Validate the lengths.
          if (password.length < 1) {
            errors.password = "Password is required.";
          }
          // Validate e-mail.
          if (!validate(email)) {
            errors.email = "Invalid e-mail address.";
          }

          return errors;
        }}
        onSubmit={onSubmitHandler}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <StyledForm onSubmit={handleSubmit}>
            <Form.Group>
              <Form.Label>E-mail</Form.Label>
              <Form.Control
                type="email"
                placeholder="E-mail address..."
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                name="email"
              />
              <FieldValidationError isShown={errors.email && touched.email}>
                {errors.email}
              </FieldValidationError>
            </Form.Group>
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password..."
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                name="password"
              />
              <FieldValidationError
                isShown={errors.password && touched.password}
              >
                {errors.password}
              </FieldValidationError>
            </Form.Group>
            <ActionLink onClick={() => setModalOpen(true)}>
              Forgot your password?
            </ActionLink>
            <LoginButton
              variant="primary"
              type="submit"
              disabled={isSubmitting}
            >
              Login
            </LoginButton>
          </StyledForm>
        )}
      </Formik>
    </>
  );
};

LoginForm.propTypes = {
  onSubmitHandler: PropTypes.func.isRequired,
};

export default LoginForm;
