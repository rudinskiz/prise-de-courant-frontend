import React from "react";

import { Meta } from "@storybook/react";
import LoginForm from "./LoginForm";

export default {
  title: "Components/Forms/LoginForm",
  component: LoginForm,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <LoginForm onSubmitHandler={() => null} />
);
