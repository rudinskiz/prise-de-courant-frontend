import React from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import { PasswordResetButton, StyledForm } from "./style";
import { Form } from "react-bootstrap";
import FieldValidationError from "../../Errors/FieldValidationError";

const PasswordResetForm = ({ onSubmitHandler }) => {
  return (
    <Formik
      initialValues={{
        password: "",
      }}
      validate={({ password }) => {
        // Errors object.
        const errors: any = {};
        // Validate password.
        if (password.length < 8 || password.length > 24) {
          errors.password =
            "Password should be between 8 and 24 characters long.";
        }

        console.log(errors);

        return errors;
      }}
      onSubmit={onSubmitHandler}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <StyledForm onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Label>New password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password..."
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              name="password"
            />
            <FieldValidationError isShown={errors.password && touched.password}>
              {errors.password}
            </FieldValidationError>
          </Form.Group>
          <PasswordResetButton
            variant="primary"
            type="submit"
            disabled={isSubmitting}
          >
            Reset password
          </PasswordResetButton>
        </StyledForm>
      )}
    </Formik>
  );
};

PasswordResetForm.propTypes = {
  onSubmitHandler: PropTypes.func.isRequired,
};

export default PasswordResetForm;
