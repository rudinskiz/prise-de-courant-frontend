import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import PasswordResetForm from "./PasswordResetForm";

describe("PasswordResetForm.tsx", () => {
  it("should render properly", () => {
    const loginForm = shallow(
      <PasswordResetForm
        onSubmitHandler={() => null}
      />
    );

    expect(toJSON(loginForm)).toMatchSnapshot();
  });

  it("should validate form before calling the API", async () => {
    const submitHandler = jest.fn();

    const loginForm = mount(
      <PasswordResetForm
        onSubmitHandler={submitHandler}
      />
    );
    loginForm.find("button").simulate("click");

    expect(submitHandler).not.toBeCalled();
  });
});
