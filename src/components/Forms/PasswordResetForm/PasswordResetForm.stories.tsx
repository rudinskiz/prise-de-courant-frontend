import React from "react";

import { Meta } from "@storybook/react";
import PasswordResetForm from "./PasswordResetForm";

export default {
  title: "Components/Forms/PasswordResetForm",
  component: PasswordResetForm,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <PasswordResetForm onSubmitHandler={() => null} />
);
