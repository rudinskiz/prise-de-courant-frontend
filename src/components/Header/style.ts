import styled from "styled-components";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import theme from "../../styles/theme";

const HeaderWrapper = styled.nav`
  padding: 10px;
  background-color: ${theme.colors.primary};
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media screen and (max-width: ${theme.breakpoints.tablet}) {
    flex-direction: column;
  }
`;

const HeaderBrandWrapper = styled.div``;

const HeaderBrandImage = styled.img`
  height: 115px;
`;

const HeaderBrand = styled(Link)``;

const HeaderNavigationWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  @media screen and (min-width: ${theme.breakpoints.tablet}) {
    display: block;
  }
`;

const HeaderNavigationLink = styled(Link)<{ isActive: boolean }>`
  transition: 500ms;
  outline: 0;
  font-family: ${theme.fonts.navigationFont};
  font-weight: bold;
  color: ${(props) =>
    props.isActive ? theme.colors.secondary : theme.colors.primaryText};
  margin: 0px 10px;
  &:hover {
    color: ${(props) =>
      props.isActive ? theme.colors.secondary : theme.colors.primaryText};
  }
`;

HeaderNavigationLink.propTypes = {
  isActive: PropTypes.bool.isRequired,
};

export {
  HeaderWrapper,
  HeaderBrand,
  HeaderBrandWrapper,
  HeaderNavigationLink,
  HeaderNavigationWrapper,
  HeaderBrandImage,
};
