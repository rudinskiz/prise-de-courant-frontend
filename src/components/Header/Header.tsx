import React, { useContext } from "react";
import { useLocation } from "react-router-dom";

import {
  HeaderBrand,
  HeaderBrandImage,
  HeaderBrandWrapper,
  HeaderNavigationLink,
  HeaderNavigationWrapper,
  HeaderWrapper,
} from "./style";
import constants from "../../utils/constants";
import Logo from "../../assets/restaurant-logo.jpg";
import UserContext from "../../utils/contexts/UserContext";
import { last } from "lodash";

const Header = () => {
  const location = useLocation();
  const userContext = useContext(UserContext);
  const { user } = userContext;

  return (
    <HeaderWrapper>
      <HeaderBrandWrapper>
        <HeaderBrand to="/">
          <HeaderBrandImage src={Logo} />
        </HeaderBrand>
      </HeaderBrandWrapper>
      <HeaderNavigationWrapper>
        <HeaderNavigationLink to="/" isActive={location.pathname === "/"}>
          Home
        </HeaderNavigationLink>
        <HeaderNavigationLink
          to="/tables"
          isActive={location.pathname === "/tables"}
        >
          Tables
        </HeaderNavigationLink>
        <HeaderNavigationLink
          to="/contact"
          isActive={location.pathname === "/contact"}
        >
          Contact
        </HeaderNavigationLink>
        <HeaderNavigationLink
          to="/about-us"
          isActive={location.pathname === "/about-us"}
        >
          About Us
        </HeaderNavigationLink>
        {user !== null &&
          [
            constants.auth.adminRole,
            last(constants.auth.authenticatedRoles),
          ].includes(user.getRoleName()) && (
            <HeaderNavigationLink
              to={"/reservation/check"}
              isActive={location.pathname === "/reservation/check"}
            >
              Reservations
            </HeaderNavigationLink>
          )}
        {user !== null && (
          <>
            <HeaderNavigationLink
              to="/profile"
              isActive={location.pathname === "/profile"}
            >
              My Profile
            </HeaderNavigationLink>
            <HeaderNavigationLink
              to="/logout"
              isActive={location.pathname === "/logout"}
            >
              Log out
            </HeaderNavigationLink>
          </>
        )}
        {user === null && (
          <>
            <HeaderNavigationLink
              to="/register"
              isActive={location.pathname === "/register"}
            >
              Register
            </HeaderNavigationLink>
            <HeaderNavigationLink
              to="/login"
              isActive={location.pathname === "/login"}
            >
              Login
            </HeaderNavigationLink>
          </>
        )}
      </HeaderNavigationWrapper>
    </HeaderWrapper>
  );
};

export default Header;
