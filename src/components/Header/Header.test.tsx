import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import Header from "./Header";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "/",
  }),
}));

describe("Header.tsx", () => {
  it("should render properly", () => {
    const header = shallow(<Header />);

    expect(toJSON(header)).toMatchSnapshot();
  });
});
