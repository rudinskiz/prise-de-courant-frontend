import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { darken } from "polished";
import Theme from "../../styles/theme";

const ActionLinkWrapper = styled.div`
  will-change: color;
  transition: color 200ms ease;
  color: ${Theme.colors.primary};
  cursor: pointer;
  width: max-content;

  &:hover {
    color: ${darken(0.65, Theme.colors.primaryText)};
  }
  &:active {
    color: ${darken(0.95, Theme.colors.primaryText)};
  }
`;

const ActionLink = ({ onClick, children }) => {
  return <ActionLinkWrapper onClick={onClick}>{children}</ActionLinkWrapper>;
};

ActionLink.propTypes = {
  children: PropTypes.any,
  onClick: PropTypes.func.isRequired,
};

export default ActionLink;
