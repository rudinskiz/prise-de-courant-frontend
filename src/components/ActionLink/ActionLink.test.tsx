import React from "react";
import { mount, shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import ActionLink from "./ActionLink";

describe("ActionLink.tsx", () => {
  it("should render properly", () => {
    const link = shallow(<ActionLink onClick={() => null}>Jest</ActionLink>);

    expect(toJSON(link)).toMatchSnapshot();
  });

  it("should update properly", () => {
    const onClick = jest.fn();

    const link = mount(<ActionLink onClick={onClick}>Jest</ActionLink>);

    link.simulate("click");

    expect(onClick).toBeCalledTimes(1);
  });
});
