import React from "react";

import { Meta } from "@storybook/react";
import ActionLink from "./ActionLink";

export default {
  title: "Components/ActionLink",
  component: ActionLink,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <ActionLink onClick={() => null}>Click me.</ActionLink>
);
