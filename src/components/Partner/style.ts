import styled from "styled-components";

export const PartnerImage = styled.img`
  margin: 10px 0;
  max-width: 100%;
`;