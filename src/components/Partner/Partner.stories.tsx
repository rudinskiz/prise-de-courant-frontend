import React from "react";

import { Meta } from "@storybook/react";
import Partner from "./Partner";

import Makova from "../../assets/images/makova.png";

export default {
  title: "Components/Partner",
  component: Partner,
} as Meta;

export const Default: React.VFC<{}> = () => (
  <Partner src={Makova} alt="Makova" url="http://umpc.proj.vts.su.ac.rs/" />
);
