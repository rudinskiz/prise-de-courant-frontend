import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import Partner from "./Partner";

const mockProps = {
  url: "https://www.google.com",
  src: "https://www.google.com",
  alt: "Google",
};

describe("Partner.tsx", () => {
  it("should render properly", () => {
    const partner = shallow(<Partner {...mockProps} />);

    expect(toJSON(partner)).toMatchSnapshot();
  });
});
