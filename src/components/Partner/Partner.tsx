import { PartnerImage } from "./style";

function Partner({ src, alt, url }: { src: string; alt: string; url: string }) {
  return (
    <a href={url} target="_blank" rel="noopener noreferrer">
      <PartnerImage src={src} alt={alt} />
    </a>
  );
}

export default Partner;
