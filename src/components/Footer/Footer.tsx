import React from "react";
import { FooterWrapper } from "./style";

const Footer = () => {
  return (
    <FooterWrapper>
      <span>Vrili Kodovi &copy; 2021</span>
    </FooterWrapper>
  );
};

export default Footer;
