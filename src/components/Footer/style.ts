import styled from 'styled-components';
import Theme from '../../styles/theme';

export const FooterWrapper = styled.footer`
    background: ${Theme.colors.primary};
    color: ${Theme.colors.primaryText};
    text-align: right;
    padding: 1rem;
    font-family: ${Theme.fonts.fancyFont};
`;