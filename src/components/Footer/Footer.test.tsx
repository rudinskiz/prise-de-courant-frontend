import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import Footer from "./Footer";

describe("Footer.tsx", () => {
  it("should render properly", () => {
    const footer = shallow(
      <Footer />
    );

    expect(toJSON(footer)).toMatchSnapshot();
  });
});
