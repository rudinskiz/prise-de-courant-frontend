import { Helmet } from "react-helmet";

function Title({ title }: { title: string }) {
  return (
    <Helmet>
      <title>{title} | Prise De Courant</title>
    </Helmet>
  );
}

export default Title;
