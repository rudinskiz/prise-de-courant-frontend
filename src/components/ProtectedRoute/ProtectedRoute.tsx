import React, { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Redirect, Route } from "react-router";
import Loader from "../Loader/Loader";
import UserContext from "../../utils/contexts/UserContext";

const ProtectedRoute = ({
  acceptableRoles,
  unacceptableRoles,
  children,
  path,
  exact,
  redirectPath,
  unacceptablePath,
}) => {
  const [routeRender, setRouteRender] = useState(
    <Loader isLoading={true}>{children}</Loader>
  );
  const { user } = useContext(UserContext);

  useEffect(() => {
    // Get user role.
    const role = user?.getRoleName() ?? "";
    // Check for unacceptable roles.
    if (unacceptableRoles.includes(role)) {
      setRouteRender(<Redirect to={unacceptablePath} />);
    } else {
      // Check for acceptable roles.
      if (
        acceptableRoles.length === 0 ||
        acceptableRoles.includes(user?.getRoleName() ?? "")
      ) {
        setRouteRender(<Loader isLoading={false}>{children}</Loader>);
      } else {
        setRouteRender(<Redirect to={redirectPath} />);
      }
    }
  }, [
    user,
    children,
    acceptableRoles,
    unacceptableRoles,
    redirectPath,
    unacceptablePath,
  ]);

  return (
    <Route
      path={path}
      exact={exact}
      render={() => {
        return routeRender;
      }}
    />
  );
};

ProtectedRoute.defaultProps = {
  unacceptableRoles: [],
  acceptableRoles: [],
  children: null,
  unacceptablePath: "/access-denied",
  redirectPath: "/login",
  exact: false,
};

ProtectedRoute.propTypes = {
  unacceptableRoles: PropTypes.arrayOf(PropTypes.string),
  acceptableRoles: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.any,
  path: PropTypes.string.isRequired,
  unacceptablePath: PropTypes.string,
  redirectPath: PropTypes.string,
  exact: PropTypes.bool,
};

export default ProtectedRoute;
