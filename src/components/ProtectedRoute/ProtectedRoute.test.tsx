import React from "react";
import { mount } from "enzyme";
import toJSON from "enzyme-to-json";
import { act } from "react-dom/test-utils";
import ProtectedRoute from "./ProtectedRoute";
import { BrowserRouter, Route } from "react-router-dom";

describe("ProtectedRoute.tsx", () => {
  it("should render properly", async () => {
    let route;
    await act(async () => {
      route = mount(
        <BrowserRouter>
          <ProtectedRoute path="/" />
        </BrowserRouter>
      );
    });
    route.update();

    expect(toJSON(route.find(ProtectedRoute))).toMatchSnapshot();
  });

  it("should redirect when user doesn't have an acceptable role", async () => {
    let route;
    await act(async () => {
      route = mount(
        <BrowserRouter>
          <ProtectedRoute path="/" acceptableRoles={["mockRole"]} />
        </BrowserRouter>
      );
    });
    route.update();

    expect(toJSON(route.find(ProtectedRoute))).toMatchSnapshot();
  });
});
