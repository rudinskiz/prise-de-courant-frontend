import React from "react";

import { Meta } from "@storybook/react";
import PageTitle from "./PageTitle";

export default {
  title: "Components/PageTitle",
  component: PageTitle,
} as Meta;

export const Left: React.VFC<{}> = () => <PageTitle align="left">Page title</PageTitle>;
export const Center: React.VFC<{}> = () => <PageTitle align="center">Page title</PageTitle>;
export const Right: React.VFC<{}> = () => <PageTitle align="right">Page title</PageTitle>;
