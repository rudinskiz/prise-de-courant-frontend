import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import PageTitle from "./PageTitle";

const mockTitle = "Jest";

describe("PageTitle.tsx", () => {
  it("should render properly", () => {
    const pageTitle = shallow(
      <PageTitle align="center">{mockTitle}</PageTitle>
    );

    expect(toJSON(pageTitle)).toMatchSnapshot();
    expect(pageTitle.text()).toBe(mockTitle);
  });
});
