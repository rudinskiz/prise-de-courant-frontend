import styled from 'styled-components';
import Theme from '../../styles/theme';

const PageTitle = styled.h1<{ align: string }>`
    margin-top: 10px;
    text-align: ${props => props.align};
    text-decoration: underline;
    font-family: ${Theme.fonts.fancyFont};
`;

export default PageTitle;