import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import Modal from "./Modal";

describe("Modal.tsx", () => {
  it("should render properly", () => {
    const modal = shallow(
      <Modal isOpen={true} setModalState={() => null}>
        hello
      </Modal>
    );

    expect(toJSON(modal)).toMatchSnapshot();
  });
});
