import React from "react";
import PropTypes from "prop-types";
import Modal from "../Modal";
import ConfirmForm from "../../Forms/ConfirmForm/ConfirmForm";

const ConfirmModal = ({ isOpen, setModalState, action }) => {
  return (
    <Modal isOpen={isOpen} setModalState={setModalState}>
      <ConfirmForm action={action} cancel={() => setModalState(false)} />
    </Modal>
  );
};

ConfirmModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  action: PropTypes.func.isRequired,
  setModalState: PropTypes.func.isRequired,
};

export default ConfirmModal;
