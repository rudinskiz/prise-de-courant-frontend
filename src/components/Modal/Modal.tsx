import React from "react";
import ReactModal from "react-modal";
import PropTypes from "prop-types";
import styled from "styled-components";
import Close from "../../assets/close.png";

const StyledModal = styled(ReactModal)`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  background: white;
  min-width: 50vw;
  border: 1px solid black;
  padding: 30px;
  border-radius: 8px;
`;

const CloseIcon = styled.div`
  cursor: pointer;
  position: absolute;
  bottom: calc(100% - 16px);
  left: calc(100% - 16px);
  width: 32px;
  img {
    margin: 0 auto;
    width: 32px;
  }
`;

const Modal = ({
  isOpen,
  onAfterOpen,
  onRequestClose,
  ariaHideApp,
  children,
  setModalState,
}) => {
  return (
    <>
      <StyledModal
        isOpen={isOpen}
        onAfterOpen={onAfterOpen}
        onRequestClose={onRequestClose}
        ariaHideApp={ariaHideApp}
        closeTimeoutMS={500}
      >
        <CloseIcon
          className={"closeModal"}
          onClick={() => {
            setModalState(false);
          }}
        >
          <img src={Close} alt="Close" />
        </CloseIcon>
        {children}
      </StyledModal>
    </>
  );
};

Modal.defaultProps = {
  onAfterOpen: () => null,
  onRequestClose: () => null,
  ariaHideApp: false,
};

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onAfterOpen: PropTypes.func,
  onRequestClose: PropTypes.func,
  ariaHideApp: PropTypes.bool,
  setModalState: PropTypes.func.isRequired,
};

export default Modal;
