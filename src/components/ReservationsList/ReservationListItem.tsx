import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ActionButton } from "../Forms/ReservationForm/style";
import { cancelReservation } from "../../utils/api/reservation";
import { useHistory } from "react-router-dom";
import Reservation from "../../models/Reservation";

const Wrapper = styled.div`
  display: flex;
  gap: 20px;
  align-items: baseline;
  justify-content: space-between;
`;

const CancelButton = styled(ActionButton)`
  max-width: 30%;
`;

const ReservationListItem = ({ reservation }: { reservation: Reservation }) => {
  const [isSubmitting, setIsSubmitting] = useState(false);
  const history = useHistory();

  const onCancelHandler = (e) => {
    e.preventDefault();
    cancelReservation(reservation.getCode()).then(() => {
      history.go(0);
    });
  };

  useEffect(() => {
    const reservationDateLimit = reservation.getStartDate();
    const diff = reservationDateLimit.diffNow("hours").get("hours");

    console.log(diff);

    setIsSubmitting(diff <= 4);
    // eslint-disable-next-line
  }, [reservation]);

  return (
    <Wrapper>
      <div>{reservation.label()}</div>
      <CancelButton onClick={onCancelHandler} disabled={isSubmitting}>
        Cancel
      </CancelButton>
    </Wrapper>
  );
};

ReservationListItem.propTypes = {
  reservation: PropTypes.any,
};

export default ReservationListItem;
