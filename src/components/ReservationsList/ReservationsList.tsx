import React, { useEffect, useState } from "react";
import useAuthentication from "../../hooks/useAuthentication";
import { getAllReservations } from "../../utils/api/reservation";
import PageTitle from "../PageTitle/PageTitle";
import ReservationListItem from "./ReservationListItem";
import DatePicker from "react-date-picker";
import { DateTime } from "luxon";
import Reservation from "../../models/Reservation";

const ReservationsList = () => {
  const { getToken } = useAuthentication();
  const [reservations, setReservations] = useState([]);
  const [datePickerValue, setDatePickerValue] = useState(DateTime.now());

  const onDateChange = (value) => {
    setDatePickerValue(DateTime.fromJSDate(value));
  };

  useEffect(() => {
    getAllReservations(getToken()?.getValue() ?? "").then(
      (reservations: Reservation[]) => {
        setReservations(
          reservations.filter((r) => {
            const days = Math.floor(
              r.getStartDate().diff(datePickerValue, "days").days
            );
            return days === 0;
          })
        );
      }
    );
    // eslint-disable-next-line
  }, [datePickerValue]);

  return (
    <>
      <PageTitle align={"left"}>Reservations:</PageTitle>
      <DatePicker
        clearIcon={null}
        onChange={onDateChange}
        value={datePickerValue.toJSDate()}
      />
      {reservations.map((reservation) => (
        <ReservationListItem reservation={reservation} />
      ))}
    </>
  );
};

export default ReservationsList;
