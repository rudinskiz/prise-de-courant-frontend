import Token from "../models/Token";
import constants from "../utils/constants";
import { DateTime } from "luxon";

const useAuthentication = () => {
  const getToken = () => {
    const tokenInformation: string =
      localStorage.getItem(`${constants.auth.prefix}user`) ?? null;
    if (tokenInformation === null) {
      return null;
    }
    const tokenInfo: { value: string; expirationDate: string } = JSON.parse(
      tokenInformation
    );
    const { value, expirationDate } = tokenInfo;

    return new Token(
      value,
      DateTime.fromFormat(expirationDate, "yyyy-MM-dd HH:mm:ss", {
        zone: "UTC",
      })
    );
  };

  const setToken = (token: Token) => {
    localStorage.setItem(`${constants.auth.prefix}user`, JSON.stringify(token));
  };

  const deleteToken = () => {
    localStorage.removeItem(`${constants.auth.prefix}user`);
  };

  return {
    getToken,
    setToken,
    deleteToken,
  };
};

export default useAuthentication;
