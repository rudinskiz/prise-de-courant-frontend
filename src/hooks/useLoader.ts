import { Dispatch, SetStateAction, useState } from 'react';

const useLoader = (): [boolean, Dispatch<SetStateAction<boolean>>] => {
    const [loading, setLoading] = useState(true);

    return [loading, setLoading];
};

export default useLoader;