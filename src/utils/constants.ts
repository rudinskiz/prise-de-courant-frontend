const Constants = {
  apiUrl: "http://localhost:8000/api",
  auth: {
    prefix: "PRISE-",
    authenticatedRoles: ["Administrator", "Authenticated", "Restaurant Worker"],
    regularRole: "Authenticated",
    adminRole: "Administrator",
  },
};

export default Constants;
