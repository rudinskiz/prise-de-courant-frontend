import axios from "axios";
import { DateTime } from "luxon";
import Role from "../../models/Role";
import Token from "../../models/Token";
import User from "../../models/User";
import Constants from "../constants";

const register = async (
  values
): Promise<{ status: boolean; errorMessage: string }> => {
  // Call the API method.
  try {
    await axios.post(`${Constants.apiUrl}/user/register`, values);

    return {
      status: true,
      errorMessage: "",
    };
  } catch (e) {
    const { message: errorMessage } = e?.response?.data ?? {
      message: "",
    };

    return {
      status: false,
      errorMessage,
    };
  }
};

const login = async (values): Promise<{ status: boolean; data: any }> => {
  // Call the API method.
  try {
    const {
      data: { data },
    } = await axios.post(`${Constants.apiUrl}/user/login`, values);

    return {
      status: true,
      data,
    };
  } catch {
    return {
      status: false,
      data: [],
    };
  }
};

const verify = async (token: string): Promise<boolean> => {
  // Call the API method.
  if (!token) {
    return false;
  }
  try {
    await axios.get(
      `${Constants.apiUrl}/user/verifyAccount?token=${encodeURIComponent(
        token
      )}`
    );

    return true;
  } catch {
    return false;
  }
};

const getUserInformation = async (token: string): Promise<User> => {
  // Call the API method.
  if (!token) {
    return null;
  }
  try {
    const {
      data: { data },
    } = await axios.get(
      `${Constants.apiUrl}/user/getInformation?token=${encodeURIComponent(
        token
      )}`
    );

    // Extract user information.
    const { email, name, phoneNumber, role } = data;
    return new User(email, name, phoneNumber, new Role(role));
  } catch {
    return null;
  }
};

const requestPasswordReset = async (email: string): Promise<boolean> => {
  // Call the API method.
  try {
    await axios.get(
      `${Constants.apiUrl}/user/resetPassword?email=${encodeURIComponent(
        email
      )}`
    );
    return true;
  } catch {
    return false;
  }
};

const resetPassword = async (values: {
  password: string;
  token: string;
}): Promise<boolean> => {
  const { token } = values;
  // Call the API method.
  if (!token) {
    return false;
  }
  try {
    await axios.post(`${Constants.apiUrl}/user/resetPassword`, values);
    return true;
  } catch {
    return false;
  }
};

const requestVerificationResend = async (email: string): Promise<boolean> => {
  // Call the API method.
  try {
    await axios.get(
      `${Constants.apiUrl}/user/resendVerification?email=${encodeURIComponent(
        email
      )}`
    );
    return true;
  } catch {
    return false;
  }
};

const updateUserInformation = async (
  values,
  token: string
): Promise<boolean> => {
  // Call the API method.
  if (!token) {
    return false;
  }
  try {
    await axios.post(`${Constants.apiUrl}/user/updateInformation`, {
      token,
      values,
    });
    return true;
  } catch {
    return false;
  }
};

const masquerade = async (email: string, token: string) => {
  if (!token) {
    return null;
  }
  try {
    const {
      data: {
        data: { token: accessToken, expirationDate },
      },
    } = await axios.post(`${Constants.apiUrl}/user/masquerade`, {
      email,
      token,
    });

    return new Token(
      accessToken,
      DateTime.fromFormat(expirationDate, "yyyy-MM-dd HH:mm:ss", {
        zone: "UTC",
      })
    );
  } catch {
    return null;
  }
};

export {
  register,
  login,
  verify,
  getUserInformation,
  requestPasswordReset,
  resetPassword,
  requestVerificationResend,
  updateUserInformation,
  masquerade,
};
