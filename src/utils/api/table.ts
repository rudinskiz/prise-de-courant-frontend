import axios from "axios";
import Table from "../../models/Table";
import Constants from "../constants";

const getTable = async (tableId: number): Promise<Table> => {
  try {
    const {
      data: { data },
    } = await axios.get(
      `${Constants.apiUrl}/table/getTableInformation?tableId=${tableId}`
    );

    return new Table(
      data.tableId,
      data.tableName,
      data.tableDescription,
      data.numberOfSeats,
      data.smokingAllowed
    );
  } catch {
    return null;
  }
};

const getAllTables = async (
  offset: number = 0,
  limit: number = 10,
  filterName: string = ""
): Promise<{ data: Array<Table>; count: number }> => {
  try {
    const {
      data: {
        data: { data, count },
      },
    } = await axios.get(
      `${
        Constants.apiUrl
      }/table/getTablesInformation?limit=${limit}&skip=${offset}${
        filterName.length > 0 ? `&filter=${encodeURIComponent(filterName)}` : ""
      }`
    );
    return {
      count,
      data: data.map(
        (table) =>
          new Table(
            table.tableId,
            table.tableName,
            table.tableDescription,
            table.numberOfSeats,
            table.smokingAllowed
          )
      ),
    };
  } catch {
    return {
      count: -1,
      data: [],
    };
  }
};

const addTable = async (values): Promise<boolean> => {
  // Call the API method.
  const { token } = values;
  if (!token) {
    return false;
  }
  try {
    await axios.post(`${Constants.apiUrl}/table/createTable`, values);

    return true;
  } catch {
    return false;
  }
};

const updateTable = async (values): Promise<boolean> => {
  // Call the API method.
  const { token } = values;
  if (!token) {
    return false;
  }
  try {
    await axios.post(
      `${Constants.apiUrl}/table/updateTableInformation`,
      values
    );

    return true;
  } catch {
    return false;
  }
};

const deleteTable = async (values): Promise<boolean> => {
  // Call the API method.
  const { token, tableId } = values;
  if (!token || !tableId) {
    return false;
  }
  try {
    await axios.get(
      `${Constants.apiUrl}/table/deleteTable?tableId=${tableId}&token=${token}`
    );

    return true;
  } catch {
    return false;
  }
};

export { getTable, getAllTables, addTable, updateTable, deleteTable };
