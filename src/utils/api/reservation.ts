import axios from "axios";
import User from "../../models/User";
import Role from "../../models/Role";
import Table from "../../models/Table";
import Reservation from "../../models/Reservation";
import Constants from "../constants";
import { DateTime } from "luxon";

export const setComment = async (values) => {
  const { token } = values;
  if (!token) {
    return false;
  }
  try {
    await axios.post(`${Constants.apiUrl}/reservation/comment`, values);
    return true;
  } catch {
    return false;
  }
};

export const getAllReservations = async (token: string) => {
  if (!token) {
    return [];
  }
  try {
    const {
      data: { data },
    } = await axios.get(`${Constants.apiUrl}/reservations?token=${token}`);
    return data.map((r) => {
      const {
        user: { email, name, phone, role },
        table: {
          tableId,
          tableName,
          tableDescription,
          numberOfSeats,
          smokingAllowed,
        },
        reservationCode,
        comment,
        state,
        discount,
        start,
        end,
      } = r;

      const reservationUser = new User(email, name, phone, new Role(role));
      const reservedTable = new Table(
        tableId,
        tableName,
        tableDescription,
        numberOfSeats,
        smokingAllowed
      );

      return new Reservation(
        DateTime.fromFormat(start.date.split(".")[0], "yyyy-MM-dd HH:mm:ss"),
        DateTime.fromFormat(end.date.split(".")[0], "yyyy-MM-dd HH:mm:ss"),
        reservationCode,
        reservationUser,
        reservedTable,
        comment,
        state,
        discount
      );
    });
  } catch {
    return [];
  }
};

export const cancelReservation = async (code: string) => {
  if (!code) {
    return false;
  }
  try {
    await axios.post(`${Constants.apiUrl}/reservation/cancel?code=${code}`);
    return true;
  } catch {
    return false;
  }
};

export const createReservation = async (
  startDate: DateTime,
  endDate: DateTime,
  token: string,
  table: number
) => {
  if (!token) {
    return false;
  }

  try {
    const {
      data: {
        data: { reservationCode },
      },
    } = await axios.post(`${Constants.apiUrl}/reservation/create`, {
      token,
      table,
      start: startDate.set({ second: 0 }).toFormat("yyyy-MM-dd HH:mm:ss"),
      end: endDate.set({ second: 0 }).toFormat("yyyy-MM-dd HH:mm:ss"),
    });
    return reservationCode;
  } catch {
    return false;
  }
};

export const getReservationByCode = async (code: string) => {
  try {
    const {
      data: { data },
    } = await axios.get(
      `${Constants.apiUrl}/reservation/getByCode?code=${code}`
    );

    const {
      user: { email, name, phone, role },
      table: {
        tableId,
        tableName,
        tableDescription,
        numberOfSeats,
        smokingAllowed,
      },
      comment,
      state,
      start,
      end,
      discount,
    } = data;

    const reservationUser = new User(email, name, phone, new Role(role));
    const reservedTable = new Table(
      tableId,
      tableName,
      tableDescription,
      numberOfSeats,
      smokingAllowed
    );

    return new Reservation(
      DateTime.fromFormat(start.date.split(".")[0], "yyyy-MM-dd HH:mm:ss"),
      DateTime.fromFormat(end.date.split(".")[0], "yyyy-MM-dd HH:mm:ss"),
      code,
      reservationUser,
      reservedTable,
      comment,
      state,
      discount
    );
  } catch (err) {
    console.log(err);
    return null;
  }
};

export const addDiscount = async (code: string) => {
  if (!code) {
    return false;
  }
  try {
    await axios.post(
      `${Constants.apiUrl}/reservation/applyDiscount?code=${code}`
    );
    return true;
  } catch {
    return false;
  }
};
