import { useEffect } from "react";
import styled from "styled-components";
import Loader from "../components/Loader/Loader";
import PageTitle from "../components/PageTitle/PageTitle";
import Title from "../components/Title/Title";
import useLoader from "../hooks/useLoader";
import MapMarkerIcon from "../assets/map-marker-icon.png";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { Icon } from "leaflet";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

function AboutPage() {
  const [loading, setLoading] = useLoader();
  const MarkerIcon = new Icon({
    iconUrl: MapMarkerIcon,
    iconSize: [32, 32],
  });

  useEffect(() => {
    // Illusion of some loading.
    setLoading(false);
  }, [setLoading]);

  return (
    <>
      <Title title="About us" />
      <Loader isLoading={loading}>
        <Wrapper>
          <PageTitle align="left">About Us</PageTitle>
          <p>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit.
            Dignissimos minus consequatur illum delectus neque, excepturi at, a
            voluptatem amet earum ex dolor aperiam sed voluptates distinctio
            quibusdam! Architecto, facere corrupti!
          </p>
          <b>You can find us here:</b>
          <MapContainer
            center={[46.1008381, 19.6525828]}
            zoom={20}
            style={{ height: "600px" }}
            scrollWheelZoom={false}
          >
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker position={[46.1008381, 19.6525828]} icon={MarkerIcon}>
              <Popup>
                <b>Prise de Courant</b>
                <p>Karadjordjev put 9</p>
              </Popup>
            </Marker>
          </MapContainer>
        </Wrapper>
      </Loader>
    </>
  );
}

export default AboutPage;
