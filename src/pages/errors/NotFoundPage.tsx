import { useEffect } from "react";
import styled from "styled-components";
import Loader from "../../components/Loader/Loader";
import useLoader from "../../hooks/useLoader";
import NotFound from "../../assets/404.svg";
import Title from "../../components/Title/Title";

const Image = styled.img`
  margin-top: 5rem;
  max-width: 100%;
  max-height: 55vh;
  display: block;
  margin: auto;
`;

const Wrapper = styled.div`
  display: flex;
`;

const NotFoundPage = () => {
  const [loading, setLoading] = useLoader();

  useEffect(() => {
    // Illusion of some loading.
    setLoading(false);
  }, [setLoading]);

  return (
    <>
      <Title title="404" />
      <Loader isLoading={loading}>
        <Wrapper>
          <Image src={NotFound} />
        </Wrapper>
      </Loader>
    </>
  );
};

export default NotFoundPage;
