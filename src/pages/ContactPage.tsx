import { useEffect } from "react";
import styled from "styled-components";
import Loader from "../components/Loader/Loader";
import PageTitle from "../components/PageTitle/PageTitle";
import Title from "../components/Title/Title";
import useLoader from "../hooks/useLoader";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

const ContactInformation = styled.div`
  display: flex;
  flex-direction: row;
  gap: 5px;
`;

function ContactPage() {
  const [loading, setLoading] = useLoader();

  useEffect(() => {
    // Illusion of some loading.
    setLoading(false);
  }, [setLoading]);

  return (
    <>
      <Title title="Contact" />
      <Loader isLoading={loading}>
        <Wrapper>
          <PageTitle align="left">Contact Us</PageTitle>
          <ContactInformation>
            <span>Our phone number:</span>
            <a href="tel:+381653206201">+38165/320-6201</a>
          </ContactInformation>
          <ContactInformation>
            <span>Our email:</span>
            <a href="mailto:zvonimir@mailfence.com">admin@prisedecourant.fr</a>
          </ContactInformation>
        </Wrapper>
      </Loader>
    </>
  );
}

export default ContactPage;
