import ReservationForm from "../../components/Forms/ReservationForm/ReservationForm";

const ReservationPage = () => {
  return <ReservationForm />;
};

export default ReservationPage;
