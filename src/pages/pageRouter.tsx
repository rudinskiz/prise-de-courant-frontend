import React, { useEffect, useState } from "react";
import { Switch } from "react-router-dom";
import styled from "styled-components";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import ProtectedRoute from "../components/ProtectedRoute/ProtectedRoute";
import useAuthentication from "../hooks/useAuthentication";
import { getUserInformation } from "../utils/api/user";
import UserContext from "../utils/contexts/UserContext";
import LoginPage from "./account/LoginPage";
import LogoutPage from "./account/LogoutPage";
import RegistrationPage from "./account/RegistrationPage";
import VerificationPage from "./account/VerificationPage";
import NotFoundPage from "./errors/NotFoundPage";
import constants from "../utils/constants";
import ProfilePage from "./account/ProfilePage";
import PasswordResetPage from "./account/PasswordResetPage";
import AccessDeniedPage from "./errors/AccesDeniedPage";
import AllTablesPage from "./table/AllTablesPage";
import TablePage from "./table/TablePage";
import AddTablePage from "./table/AddTablePage";
import EditTablePage from "./table/EditTablePage";
import ReservationPage from "./reservation/ReservationPage";
import { last } from "lodash";
import ContactPage from "./ContactPage";
import AboutPage from "./AboutPage";
import HomePage from "./HomePage";

const SwitchWrapper = styled.div``;

const PageRouter = () => {
  const { getToken } = useAuthentication();
  const [userInfo, setUserInfo] = useState(undefined);
  const defaultValues = { user: userInfo, setUser: setUserInfo };

  useEffect(() => {
    async function getUser() {
      const data = await getUserInformation(getToken()?.getValue() ?? "");
      return data;
    }

    getUser().then((data) => {
      setUserInfo(data);
    });
    // eslint-disable-next-line
  }, []);

  return (
    <UserContext.Provider value={defaultValues}>
      {userInfo !== undefined && (
        <>
          <Header />
          <SwitchWrapper>
            <Switch>
              {/* GENERAL ROUTES */}
              <ProtectedRoute exact path="/">
                <HomePage />
              </ProtectedRoute>
              <ProtectedRoute path="/contact">
                <ContactPage />
              </ProtectedRoute>
              <ProtectedRoute path="/about-us">
                <AboutPage />
              </ProtectedRoute>
              {/* USER ROUTES */}
              <ProtectedRoute
                unacceptableRoles={constants.auth.authenticatedRoles}
                path="/login"
              >
                <LoginPage />
              </ProtectedRoute>
              <ProtectedRoute
                unacceptableRoles={constants.auth.authenticatedRoles}
                path="/register"
              >
                <RegistrationPage />
              </ProtectedRoute>
              <ProtectedRoute
                acceptableRoles={constants.auth.authenticatedRoles}
                path="/logout"
              >
                <LogoutPage />
              </ProtectedRoute>
              <ProtectedRoute
                acceptableRoles={constants.auth.authenticatedRoles}
                path="/profile"
              >
                <ProfilePage />
              </ProtectedRoute>
              <ProtectedRoute
                unacceptableRoles={constants.auth.authenticatedRoles}
                path="/verification"
              >
                <VerificationPage />
              </ProtectedRoute>
              <ProtectedRoute
                unacceptableRoles={constants.auth.authenticatedRoles}
                path="/resetPassword"
              >
                <PasswordResetPage />
              </ProtectedRoute>
              {/* TABLE ROUTES */}
              <ProtectedRoute path="/tables">
                <AllTablesPage />
              </ProtectedRoute>
              <ProtectedRoute
                path="/table/add"
                exact
                acceptableRoles={[constants.auth.adminRole]}
              >
                <AddTablePage />
              </ProtectedRoute>
              <ProtectedRoute path="/table/:tableId/edit">
                <EditTablePage />
              </ProtectedRoute>
              <ProtectedRoute path="/table/:tableId">
                <TablePage />
              </ProtectedRoute>
              {/* RESERVATION ROUTES */}
              <ProtectedRoute
                exact
                acceptableRoles={[
                  constants.auth.adminRole,
                  last(constants.auth.authenticatedRoles),
                ]}
                path="/reservation/check"
              >
                <ReservationPage />
              </ProtectedRoute>
              {/* ERROR ROUTES */}
              <ProtectedRoute path="/access-denied">
                <AccessDeniedPage />
              </ProtectedRoute>
              <ProtectedRoute path="/">
                <NotFoundPage />
              </ProtectedRoute>
            </Switch>
          </SwitchWrapper>
          <Footer />
        </>
      )}
    </UserContext.Provider>
  );
};

export default PageRouter;
