import ProfileForm from "../../components/Forms/ProfileForm/ProfileForm";

const ProfilePage = () => {
  return <ProfileForm />;
};

export default ProfilePage;
