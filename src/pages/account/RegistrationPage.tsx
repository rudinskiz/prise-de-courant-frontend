import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import Loader from "../../components/Loader/Loader";
import PageTitle from "../../components/PageTitle/PageTitle";
import useLoader from "../../hooks/useLoader";
import { register } from "../../utils/api/user";
import RegisterForm from "../../components/Forms/RegisterForm/RegisterForm";
import ErrorMessage from "../../components/Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage/SuccessMessage";
import Title from "../../components/Title/Title";

const RegistrationPage = () => {
  const [registerMessage, setRegisterMessage] = useState({
    status: "",
    message: "",
  });
  const [loading, setLoading] = useLoader();

  const onSubmitHandler = (values, { setSubmitting }) => {
    // Send a register request.
    setLoading(true);
    register(values).then(({ status, errorMessage }) => {
      if (!status) {
        setRegisterMessage({
          status: "error",
          message: errorMessage,
        });
      } else {
        setRegisterMessage({
          status: "success",
          message:
            "Registration successful. Please check your inbox for confirmation e-mail.",
        });
      }

      setSubmitting(false);
      setLoading(false);
    });
  };

  useEffect(() => {
    // Illusion of loading.
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, [setLoading]);

  return (
    <>
      <Title title="Registration" />
      <Loader isLoading={loading}>
        <Container>
          <PageTitle align="left">Register</PageTitle>
          {registerMessage.message &&
            (registerMessage.status === "error" ? (
              <ErrorMessage>{registerMessage.message}</ErrorMessage>
            ) : (
              <SuccessMessage>{registerMessage.message}</SuccessMessage>
            ))}
          <RegisterForm onSubmitHandler={onSubmitHandler} />
        </Container>
      </Loader>
    </>
  );
};

export default RegistrationPage;
