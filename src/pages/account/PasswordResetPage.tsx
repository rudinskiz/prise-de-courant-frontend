import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useHistory, useLocation } from "react-router";
import { resetPassword } from "../../utils/api/user";
import PasswordResetForm from "../../components/Forms/PasswordResetForm/PasswordResetForm";
import Loader from "../../components/Loader/Loader";
import ErrorMessage from "../../components/Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage/SuccessMessage";
import PageTitle from "../../components/PageTitle/PageTitle";
import useLoader from "../../hooks/useLoader";
import Title from "../../components/Title/Title";

const PasswordResetPage = () => {
  const history = useHistory();
  const [loading, setLoading] = useLoader();
  const location = useLocation();
  const [passwordResetMessage, setPasswordResetMessage] = useState({
    status: "",
    message: "",
  });
  // Get token.
  const resetToken = new URLSearchParams(location.search).get("token");

  const onSubmitPasswordReset = (values, { setSubmitting }) => {
    resetPassword({
      password: values.password,
      token: resetToken,
    }).then((result) => {
      if (result) {
        setPasswordResetMessage({
          status: "success",
          message: "Password successfully changed. Redirecting to login...",
        });

        setTimeout(() => {
          history.push("/login");
        }, 5000);
      } else {
        setPasswordResetMessage({
          status: "error",
          message: "Something went wrong. Please try again later.",
        });

        setSubmitting(false);
      }
    });
  };

  useEffect(() => {
    setLoading(false);
  });

  return (
    <>
      <Title title="Password reset" />
      <Loader isLoading={loading}>
        <Container>
          <PageTitle align="left">Password reset</PageTitle>
          {passwordResetMessage.message &&
            (passwordResetMessage.status === "error" ? (
              <ErrorMessage>{passwordResetMessage.message}</ErrorMessage>
            ) : (
              <SuccessMessage>{passwordResetMessage.message}</SuccessMessage>
            ))}
          <PasswordResetForm onSubmitHandler={onSubmitPasswordReset} />
        </Container>
      </Loader>
    </>
  );
};

export default PasswordResetPage;
