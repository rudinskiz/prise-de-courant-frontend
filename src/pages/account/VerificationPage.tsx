import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useLocation } from "react-router";
import Loader from "../../components/Loader/Loader";
import ErrorMessage from "../../components/Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage/SuccessMessage";
import Title from "../../components/Title/Title";
import useLoader from "../../hooks/useLoader";
import { verify } from "../../utils/api/user";

const VerificationPage = () => {
  const [verificationMessage, setVerificationMessage] = useState({
    status: "success",
    message: "",
  });
  const [loading, setLoading] = useLoader();
  const location = useLocation();
  // Get token.
  const accountToken = new URLSearchParams(location.search).get("token");

  useEffect(() => {
    verify(accountToken).then((success) => {
      if (success) {
        setVerificationMessage({
          status: "success",
          message: "Account verified. You may now log in.",
        });
      } else {
        setVerificationMessage({
          status: "error",
          message: "Something went wrong. Please try again later.",
        });
      }
      setLoading(false);
    });
  }, [accountToken, setLoading, setVerificationMessage]);

  return (
    <>
      <Title title="Verifying..." />
      <Loader isLoading={loading}>
        <Container>
          {verificationMessage.message &&
            (verificationMessage.status === "error" ? (
              <ErrorMessage>{verificationMessage.message}</ErrorMessage>
            ) : (
              <SuccessMessage>{verificationMessage.message}</SuccessMessage>
            ))}
        </Container>
      </Loader>
    </>
  );
};

export default VerificationPage;
