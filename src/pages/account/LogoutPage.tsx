import { useContext, useEffect } from "react";
import { Container } from "react-bootstrap";
import { useHistory } from "react-router";
import Loader from "../../components/Loader/Loader";
import Title from "../../components/Title/Title";
import useAuthentication from "../../hooks/useAuthentication";
import useLoader from "../../hooks/useLoader";
import UserContext from "../../utils/contexts/UserContext";

const LogoutPage = () => {
  const history = useHistory();
  const { deleteToken } = useAuthentication();
  const [loading, setLoading] = useLoader();
  const userContext = useContext(UserContext);

  const { setUser } = userContext;

  useEffect(() => {
    deleteToken();
    setUser(null);
    setLoading(false);
    history.push("/");
  });

  return (
    <>
      <Title title="Logging out..." />
      <Loader isLoading={loading}>
        <Container>Logging out...</Container>
      </Loader>
    </>
  );
};

export default LogoutPage;
