import { DateTime } from "luxon";
import { useContext, useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useHistory } from "react-router";
import LoginForm from "../../components/Forms/LoginForm/LoginForm";
import Loader from "../../components/Loader/Loader";
import ErrorMessage from "../../components/Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage/SuccessMessage";
import PageTitle from "../../components/PageTitle/PageTitle";
import Title from "../../components/Title/Title";
import useAuthentication from "../../hooks/useAuthentication";
import useLoader from "../../hooks/useLoader";
import Token from "../../models/Token";
import { getUserInformation, login } from "../../utils/api/user";
import UserContext from "../../utils/contexts/UserContext";

const LoginPage = () => {
  const { setUser } = useContext(UserContext);
  const history = useHistory();
  const [loading, setLoading] = useLoader();
  const [loginMessage, setLoginMessage] = useState({
    status: "",
    message: "",
  });
  const { setToken } = useAuthentication();

  const onSubmitHandler = (values, { setSubmitting }) => {
    // Try to get an access token.
    setLoading(true);
    login(values).then(({ status, data }) => {
      if (!status) {
        setLoginMessage({
          status: "error",
          message:
            "Invalid credentials. Please check your e-mail and password.",
        });
        setLoading(false);
        setSubmitting(false);
      } else {
        // Create a token.
        setLoginMessage({
          status: "",
          message: "",
        });
        const { token, expirationDate } = data;
        const accessToken = new Token(
          token,
          DateTime.fromFormat(expirationDate, "yyyy-MM-dd HH:mm:ss", {
            zone: "UTC",
          })
        );
        setToken(accessToken);
        getUserInformation(accessToken.getValue()).then((data) => {
          setTimeout(() => {
            setUser(data);
            setTimeout(() => {
              history.push("/profile");
            }, 1);
          }, 1);
        });
      }
    });
  };

  useEffect(() => {
    // Illusion of loading.
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, [setLoading]);

  return (
    <>
      <Title title="Login" />
      <Loader isLoading={loading}>
        <Container>
          <PageTitle align="left">Login</PageTitle>
          {loginMessage.message &&
            (loginMessage.status === "error" ? (
              <ErrorMessage>{loginMessage.message}</ErrorMessage>
            ) : (
              <SuccessMessage>{loginMessage.message}</SuccessMessage>
            ))}
          <LoginForm onSubmitHandler={onSubmitHandler} />
        </Container>
      </Loader>
    </>
  );
};

export default LoginPage;
