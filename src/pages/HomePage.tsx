import { useEffect } from "react";
import styled from "styled-components";
import Loader from "../components/Loader/Loader";
import PageTitle from "../components/PageTitle/PageTitle";
import Title from "../components/Title/Title";
import useLoader from "../hooks/useLoader";

import Makova from "../assets/makova.png";
import Burgeri from "../assets/hamburgeri.png";
import Medoproduct from "../assets/medo.png";
import Partner from "../components/Partner/Partner";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 20px;
  height: 100%;
`;

const PartnersContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin-bottom: 10px;
`;

function HomePage() {
  const [loading, setLoading] = useLoader();

  useEffect(() => {
    // Illusion of some loading.
    setLoading(false);
  }, [setLoading]);

  return (
    <>
      <Title title="Home" />
      <Loader isLoading={loading}>
        <Wrapper>
          <div>
            <PageTitle align="left">Welcome to "Prise de Courant"!</PageTitle>
            <p>Only the best French restaurant in the world!</p>
          </div>
          <PartnersContainer>
            <b>Our partners:</b>
            <Partner
              src={Makova}
              alt="Makova's Beer"
              url="http://umpc.proj.vts.su.ac.rs/"
            />
            <Partner
              src={Burgeri}
              alt="Hamburgeri DP"
              url="https://nevakcinisani.proj.vts.su.ac.rs/HamburgerijaDP/"
            />
            <Partner
              src={Medoproduct}
              alt="Medoproduct"
              url="https://www.dtech.proj.vts.su.ac.rs/GoogleAnalitycs/"
            />
            <small>Note: This website is a school project.</small>
          </PartnersContainer>
        </Wrapper>
      </Loader>
    </>
  );
}

export default HomePage;
