import { isInteger } from "formik";
import React, { useContext, useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useHistory, useParams } from "react-router";
import styled from "styled-components";
import ActionLink from "../../components/ActionLink/ActionLink";
import BookTableForm from "../../components/Forms/BookTableForm/BookTableForm";
import Loader from "../../components/Loader/Loader";
import ErrorMessage from "../../components/Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage/SuccessMessage";
import ConfirmModal from "../../components/Modal/ConfirmModal/ConfirmModal";
import Modal from "../../components/Modal/Modal";
import NavigationLink from "../../components/NavigationLink/NavigationLink";
import PageTitle from "../../components/PageTitle/PageTitle";
import Title from "../../components/Title/Title";
import useAuthentication from "../../hooks/useAuthentication";
import useLoader from "../../hooks/useLoader";
import { deleteTable, getTable } from "../../utils/api/table";
import Constants from "../../utils/constants";
import UserContext from "../../utils/contexts/UserContext";

const TableInfoWrapper = styled.div`
  border: 1px solid black;
  width: max-content;
  padding: 10px;
  max-width: 100%;
`;

const TableInformation = styled.div`
  font-weight: bold;
  span {
    font-style: italic;
    font-weight: normal;
  }
`;

const TableDescription = styled(TableInformation)`
  font-style: italic;
  text-decoration: underline;
`;

const TableIllustration = styled.div`
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid black;
  border-radius: 100%;
  background-color: #6f3700;
  color: white;
  width: 128px;
  height: 128px;
  font-weight: bold;
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap-reverse;
  justify-content: space-between;
`;

const IllustrationWrapper = styled.div`
  margin: 10px auto;
`;

const MaxWidthWrapper = styled.div`
  max-width: 100%;
`;

const BackButtonWrapper = styled.div`
  margin-top: 1rem;
`;

const TablePage = () => {
  const { tableId } = useParams() as { tableId: string };
  const [table, setTable] = useState(null);
  const [loading, setLoading] = useLoader();
  const history = useHistory();
  const { getToken } = useAuthentication();
  const { user } = useContext(UserContext);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [isBookingModalOpen, setIsBookingModalOpen] = useState(null);
  const [tableDeleteMessage, setTableDeleteMessage] = useState({
    status: "",
    message: "",
  });

  const deleteTableHandler = () => {
    setIsDeleteModalOpen(false);
    setLoading(true);
    deleteTable({ token: getToken()?.getValue() ?? "", tableId }).then(
      (status) => {
        setLoading(false);
        if (!status) {
          setTableDeleteMessage({
            status: "error",
            message: "An error has occurred. Please try again.",
          });
        } else {
          setTableDeleteMessage({
            status: "success",
            message: "Table has been deleted. Redirecting...",
          });
          setTimeout(() => history.push("/tables"), 3000);
        }
      }
    );
  };

  useEffect(() => {
    if (tableId) {
      if (isInteger(tableId)) {
        getTable(Number.parseInt(tableId)).then((table) => {
          setTable(table);
          setLoading(false);
        });
      } else {
        history.push("/tables");
      }
    }
    // eslint-disable-next-line
  }, [tableId]);

  return (
    <>
      <Title title={loading ? "Loading table..." : table.getName()} />
      <Modal isOpen={isBookingModalOpen} setModalState={setIsBookingModalOpen}>
        <BookTableForm table={tableId} />
      </Modal>
      <ConfirmModal
        isOpen={isDeleteModalOpen}
        action={() => deleteTableHandler()}
        setModalState={setIsDeleteModalOpen}
      />
      <Loader isLoading={loading}>
        <Container>
          {tableDeleteMessage.message &&
            (tableDeleteMessage.status === "error" ? (
              <ErrorMessage>{tableDeleteMessage.message}</ErrorMessage>
            ) : (
              <SuccessMessage>{tableDeleteMessage.message}</SuccessMessage>
            ))}
          {table !== null && (
            <>
              <BackButtonWrapper>
                <ActionLink
                  onClick={() => {
                    history.push("/tables");
                  }}
                >
                  Back
                </ActionLink>
              </BackButtonWrapper>
              <PageTitle align="left">{table.getName()}</PageTitle>
              <Wrapper>
                <MaxWidthWrapper>
                  <TableInfoWrapper>
                    <TableDescription>
                      "{table.getDescription()}"
                    </TableDescription>
                    <TableInformation>
                      Number of seats: <span>{table.getNumberOfSeats()}</span>
                    </TableInformation>
                    <TableInformation>
                      Smoking allowed:{" "}
                      <span>{table.isSmokingAllowed() ? "Yes" : "No"}</span>
                    </TableInformation>
                  </TableInfoWrapper>
                  {Constants.auth.authenticatedRoles.includes(
                    user?.getRoleName()
                  ) && (
                    <ActionLink onClick={() => setIsBookingModalOpen(true)}>
                      Book this table
                    </ActionLink>
                  )}
                  {user?.getRoleName() === Constants.auth.adminRole && (
                    <>
                      <NavigationLink to={`/table/${tableId}/edit`}>
                        Edit this table
                      </NavigationLink>
                      <ActionLink onClick={() => setIsDeleteModalOpen(true)}>
                        Delete table
                      </ActionLink>
                    </>
                  )}
                </MaxWidthWrapper>
                <IllustrationWrapper>
                  <TableIllustration>{table.getName()}</TableIllustration>
                </IllustrationWrapper>
              </Wrapper>
            </>
          )}
        </Container>
      </Loader>
    </>
  );
};

export default TablePage;
