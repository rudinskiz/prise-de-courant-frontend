import { isInteger } from "formik";
import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useHistory, useParams } from "react-router";
import styled from "styled-components";
import ActionLink from "../../components/ActionLink/ActionLink";
import EditTableForm from "../../components/Forms/EditTableForm/EditTableForm";
import Loader from "../../components/Loader/Loader";
import ErrorMessage from "../../components/Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage/SuccessMessage";
import PageTitle from "../../components/PageTitle/PageTitle";
import Title from "../../components/Title/Title";
import useAuthentication from "../../hooks/useAuthentication";
import useLoader from "../../hooks/useLoader";
import { updateTable, getTable } from "../../utils/api/table";

const BackButtonWrapper = styled.div`
  margin-top: 1rem;
`;

const EditTablePage = () => {
  const { tableId } = useParams() as { tableId: string };
  const [table, setTable] = useState(null);
  const { getToken } = useAuthentication();
  const [loading, setLoading] = useLoader();
  const history = useHistory();
  const [tableEditMessage, setTableAddMessage] = useState({
    status: "",
    message: "",
  });

  const onSubmitHandler = (values, { setSubmitting }) => {
    setLoading(true);
    updateTable({ values, tableId, token: getToken()?.getValue() ?? "" }).then(
      (status) => {
        setLoading(false);
        if (!status) {
          setTableAddMessage({
            status: "error",
            message: "An error has occurred. Please try again.",
          });
          setSubmitting(false);
        } else {
          setTableAddMessage({
            status: "success",
            message: "Table has been updated. Redirecting...",
          });
          setTimeout(() => {
            history.push(`/table/${tableId}`);
          }, 5000);
        }
      }
    );
  };

  const prepareInitialValues = (table) => {
    return {
      tableName: table.name,
      tableDescription: table.description,
      numberOfSeats: table.numberOfSeats,
      smokingAllowed: table.smokingAllowed,
    };
  };

  useEffect(() => {
    if (isInteger(tableId)) {
      getTable(Number.parseInt(tableId)).then((table) => {
        setTable(table);
        setLoading(false);
      });
    } else {
      history.push("/tables");
    }
    // eslint-disable-next-line
  }, [tableId]);

  return (
    <>
      <Title title={loading ? "Loading table..." : table.getName()} />
      <Loader isLoading={loading}>
        <Container>
          <BackButtonWrapper>
            <ActionLink
              onClick={() => {
                history.push(`/table/${tableId}`);
              }}
            >
              Back
            </ActionLink>
          </BackButtonWrapper>
          {table !== null && (
            <PageTitle align="left">Edit "{table.name}"</PageTitle>
          )}
          {tableEditMessage.message &&
            (tableEditMessage.status === "error" ? (
              <ErrorMessage>{tableEditMessage.message}</ErrorMessage>
            ) : (
              <SuccessMessage>{tableEditMessage.message}</SuccessMessage>
            ))}
          {table !== null && (
            <EditTableForm
              onSubmitHandler={onSubmitHandler}
              initialValues={prepareInitialValues(table)}
            />
          )}
        </Container>
      </Loader>
    </>
  );
};

export default EditTablePage;
