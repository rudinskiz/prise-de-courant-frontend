import { useContext, useEffect, useState } from "react";
import { Container, Form } from "react-bootstrap";
import Loader from "../../components/Loader/Loader";
import AllTablesTable from "../../components/Table/AllTablesTable/AllTablesTable";
import useLoader from "../../hooks/useLoader";
import { getAllTables } from "../../utils/api/table";
import styled from "styled-components";
import ActionLink from "../../components/ActionLink/ActionLink";
import PageTitle from "../../components/PageTitle/PageTitle";
import UserContext from "../../utils/contexts/UserContext";
import Constants from "../../utils/constants";
import NavigationLink from "../../components/NavigationLink/NavigationLink";
import Title from "../../components/Title/Title";

const ActionWrapper = styled.div`
  display: flex;

  div {
    margin-right: 10px;
  }
`;

const AllTablesPage = () => {
  const [loading, setLoading] = useLoader();
  const [tableCount, setTableCount] = useState(0);
  const [tables, setTables] = useState([]);
  const [offset, setOffset] = useState(0);
  const [filterName, setFilterName] = useState("");
  const [isTableListLoading, setIsTableListLoading] = useState(false);
  const { user } = useContext(UserContext);

  const itemsPerPage = 10;

  const getTables = async (initialLoad: boolean = true) => {
    if (initialLoad) {
      setLoading(true);
    }
    setIsTableListLoading(true);

    const { data, count } = await getAllTables(
      offset,
      itemsPerPage,
      filterName
    );

    setTableCount(count);
    setTables(data);
    setIsTableListLoading(false);
    if (initialLoad) {
      setLoading(false);
    }
  };

  const onSubmitFilterForm = (e) => {
    e.preventDefault();
    getTables();
  };

  useEffect(() => {
    getTables();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getTables(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [offset]);

  useEffect(() => {
    setOffset(0);
    getTables(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filterName]);

  return (
    <>
      <Title title="Tables" />
      <Loader isLoading={loading}>
        <Container>
          <PageTitle align="left">Restaurant Tables</PageTitle>
          <Form onSubmit={onSubmitFilterForm}>
            <Form.Group>
              <Form.Control
                type="text"
                value={filterName}
                placeholder="Search by name..."
                onChange={(e) => setFilterName(e.target.value)}
              />
            </Form.Group>
          </Form>
          <AllTablesTable tables={tables} loading={isTableListLoading} />
          <ActionWrapper>
            {offset - itemsPerPage >= 0 && (
              <ActionLink onClick={() => setOffset(offset - itemsPerPage)}>
                Previous
              </ActionLink>
            )}
            {offset + itemsPerPage <= tableCount && (
              <ActionLink onClick={() => setOffset(offset + itemsPerPage)}>
                Next
              </ActionLink>
            )}
            {user?.getRoleName() === Constants.auth.adminRole && (
              <NavigationLink to={"/table/add"}>
                Create new table
              </NavigationLink>
            )}
          </ActionWrapper>
        </Container>
      </Loader>
    </>
  );
};

export default AllTablesPage;
