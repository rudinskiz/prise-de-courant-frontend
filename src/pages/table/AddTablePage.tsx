import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useHistory } from "react-router";
import AddTableForm from "../../components/Forms/AddTableForm/AddTableForm";
import Loader from "../../components/Loader/Loader";
import ErrorMessage from "../../components/Messages/ErrorMessage/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage/SuccessMessage";
import PageTitle from "../../components/PageTitle/PageTitle";
import Title from "../../components/Title/Title";
import useAuthentication from "../../hooks/useAuthentication";
import useLoader from "../../hooks/useLoader";
import { addTable } from "../../utils/api/table";

const AddTablePage = () => {
  const { getToken } = useAuthentication();
  const [loading, setLoading] = useLoader();
  const history = useHistory();
  const [tableAddMessage, setTableAddMessage] = useState({
    status: "",
    message: "",
  });

  const onSubmitHandler = (values, { setSubmitting }) => {
    setLoading(true);
    addTable({ ...values, token: getToken()?.getValue() ?? "" }).then(
      (status) => {
        setLoading(false);
        if (!status) {
          setTableAddMessage({
            status: "error",
            message: "An error has occurred. Please try again.",
          });
          setSubmitting(false);
        } else {
          setTableAddMessage({
            status: "success",
            message: "Table has been created. Redirecting...",
          });
          setTimeout(() => {
            history.push("/tables");
          }, 5000);
        }
      }
    );
  };

  useEffect(() => {
    setTimeout(() => setLoading(false), 500);
  });

  return (
    <>
      <Title title="Add new table" />
      <Loader isLoading={loading}>
        <Container>
          <PageTitle align="left">Add new table</PageTitle>
          {tableAddMessage.message &&
            (tableAddMessage.status === "error" ? (
              <ErrorMessage>{tableAddMessage.message}</ErrorMessage>
            ) : (
              <SuccessMessage>{tableAddMessage.message}</SuccessMessage>
            ))}
          <AddTableForm onSubmitHandler={onSubmitHandler} />
        </Container>
      </Loader>
    </>
  );
};

export default AddTablePage;
