export default class Table {
  private id: number;
  private name: string;
  private description: string;
  private numberOfSeats: number;
  private smokingAllowed: boolean;

  constructor(
    id: number,
    name: string,
    description: string,
    numberOfSeats: number,
    smokingAllowed: boolean
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.numberOfSeats = numberOfSeats;
    this.smokingAllowed = smokingAllowed;
  }

  getId(): number {
    return this.id;
  }

  getName(): string {
    return this.name;
  }

  getDescription(): string {
    return this.description;
  }

  getNumberOfSeats(): number {
    return this.numberOfSeats;
  }

  isSmokingAllowed(): boolean {
    return this.smokingAllowed;
  }
}
