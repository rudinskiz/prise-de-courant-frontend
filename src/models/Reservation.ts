import { DateTime } from "luxon";
import Table from "./Table";
import User from "./User";

export default class Reservation {
  constructor(
    private startDate: DateTime,
    private endDate: DateTime,
    private code: string,
    private user: User,
    private table: Table,
    private comment: string,
    private state: string,
    private discount: boolean
  ) {}

  label(): string {
    return `${this.getUser().getName()} - ${this.getReservedTable().getName()} (${this.startDate.toFormat(
      "dd/MM/yyyy HH:mm"
    )} - ${this.endDate.toFormat("dd/MM/yyyy HH:mm")})`;
  }

  getStartDate(): DateTime {
    return this.startDate;
  }

  getEndDate(): DateTime {
    return this.endDate;
  }

  getCode(): string {
    return this.code;
  }

  getUser(): User {
    return this.user;
  }

  getReservedTable(): Table {
    return this.table;
  }

  getComment(): string {
    return this.comment;
  }

  isCancelled(): boolean {
    return this.state !== "cancelled";
  }

  hasDiscount(): boolean {
    return this.discount;
  }
}
