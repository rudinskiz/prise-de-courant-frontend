import Role from "./Role";

export default class User {
  private email: string;
  private name: string;
  private phone: string;
  private role: Role;

  constructor(
    email: string,
    name: string,
    phone: string,
    role: Role,
  ) {
    this.email = email;
    this.name = name;
    this.phone = phone;
    this.role = role;
  }

  getEmail(): string {
    return this.email;
  }

  getName(): string {
    return this.name;
  }

  getPhoneNumber(): string {
    return this.phone;
  }

  getRole(): Role {
    return this.role;
  }

  getRoleName(): string {
    return this.getRole().getName();
  }

}
