import { DateTime } from "luxon";

export default class Token {
  private value: string;
  private expirationDate: DateTime;

  constructor(value: string, expirationDate: DateTime) {
    this.value = value;
    this.expirationDate = expirationDate;
  }

  getValue(): string {
    return this.value;
  }

  isExpired(): boolean {
    return this.expirationDate < DateTime.now();
  }
}
